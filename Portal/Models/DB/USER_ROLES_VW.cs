//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Portal.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class USER_ROLES_VW
    {
        public decimal USER_ROLES_ID { get; set; }
        public decimal ROLE_ID { get; set; }
        public decimal CREATED_BY { get; set; }
        public System.DateTime DATE_CREATED { get; set; }
        public Nullable<decimal> MODIFIED_BY { get; set; }
        public Nullable<System.DateTime> DATE_MODIFIED { get; set; }
        public decimal USER_ID { get; set; }
        public string LOGIN_ID { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string EMAIL_ID { get; set; }
        public Nullable<decimal> DEPARTMENT_ID { get; set; }
        public string ROLE_CODE { get; set; }
    }
}
