//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Portal.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class PEOPLE_ROLE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PEOPLE_ROLE()
        {
            this.CA_PEOPLE = new HashSet<CA_PEOPLE>();
            this.PEOPLE = new HashSet<Person>();
        }
    
        public decimal ROLE_ID { get; set; }
        public decimal ORG_ID { get; set; }
        public string ROLE_CODE { get; set; }
        public string ROLE_DESC { get; set; }
        public string TABLE_NAME { get; set; }
        public string EXPIRED_FLAG { get; set; }
        public Nullable<System.DateTime> DATE_EXPIRED { get; set; }
        public decimal CREATED_BY { get; set; }
        public System.DateTime DATE_CREATED { get; set; }
        public Nullable<decimal> MODIFIED_BY { get; set; }
        public Nullable<System.DateTime> DATE_MODIFIED { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CA_PEOPLE> CA_PEOPLE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Person> PEOPLE { get; set; }
    }
}
