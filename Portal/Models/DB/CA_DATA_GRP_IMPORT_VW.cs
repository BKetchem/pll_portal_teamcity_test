//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Portal.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class CA_DATA_GRP_IMPORT_VW
    {
        public decimal CA_OBJECT_ID { get; set; }
        public string CASE_NUMBER { get; set; }
        public string CASE_TYPE { get; set; }
        public decimal CASE_TYPE_ID { get; set; }
        public decimal ORG_ID { get; set; }
        public Nullable<decimal> VALUE { get; set; }
        public string GROUP_CODE { get; set; }
        public string GROUP_DESC { get; set; }
        public string SUM_FLAG { get; set; }
        public Nullable<decimal> GROUP_SUM { get; set; }
        public decimal CASE_DATA_GROUP_ID { get; set; }
        public decimal CREATED_BY { get; set; }
        public string CREATED_BY_LOGIN_ID { get; set; }
        public System.DateTime DATE_CREATED { get; set; }
        public Nullable<decimal> MODIFIED_BY { get; set; }
        public string MODIFIED_BY_LOGIN_ID { get; set; }
        public Nullable<System.DateTime> DATE_MODIFIED { get; set; }
        public decimal CA_DATA_GROUP_ID { get; set; }
        public string DETAIL_CODE { get; set; }
        public string DETAIL_DESC { get; set; }
        public decimal DETAIL_SEQUENCE { get; set; }
        public string YES_NO_FLAG { get; set; }
        public string YES_NO_VALUE { get; set; }
        public string NUMBER_FLAG { get; set; }
        public Nullable<decimal> NUMBER_VALUE { get; set; }
        public string VALUE_FLAG { get; set; }
        public Nullable<decimal> QUANTITY { get; set; }
        public Nullable<decimal> RATE { get; set; }
        public string TEXT_VALUE { get; set; }
        public string TEXT_FLAG { get; set; }
        public string DATE_FLAG { get; set; }
        public Nullable<System.DateTime> DATE_VALUE { get; set; }
        public string LIST_VALUES_FLAG { get; set; }
        public string CA_DATA_DETAIL_LIST_VALUE { get; set; }
        public string COMMENT_FLAG { get; set; }
        public string COMMENT_VALUE { get; set; }
        public string CALC_RATE_FLAG { get; set; }
        public string COLUMN_SEQUENCE { get; set; }
        public decimal CASE_DATA_DETAIL_ID { get; set; }
        public Nullable<decimal> CA_DATA_DETAIL_ID { get; set; }
        public string LIST_VALUE { get; set; }
    }
}
