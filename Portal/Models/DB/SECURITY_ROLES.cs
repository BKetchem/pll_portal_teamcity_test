//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Portal.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class SECURITY_ROLES
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SECURITY_ROLES()
        {
            this.USER_ROLES = new HashSet<USER_ROLES>();
        }
    
        public decimal ROLE_ID { get; set; }
        public decimal ORG_ID { get; set; }
        public string ROLE_CODE { get; set; }
        public string ROLE_DESC { get; set; }
        public string EXPIRED_FLAG { get; set; }
        public Nullable<System.DateTime> DATE_EXPIRED { get; set; }
        public decimal CREATED_BY { get; set; }
        public System.DateTime DATE_CREATED { get; set; }
        public Nullable<decimal> MODIFIED_BY { get; set; }
        public Nullable<System.DateTime> DATE_MODIFIED { get; set; }
        public string CASE_TYPE_ALLOWED_FLAG { get; set; }
        public string TASK_ALLOWED_FLAG { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<USER_ROLES> USER_ROLES { get; set; }
    }
}
