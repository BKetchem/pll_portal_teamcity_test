//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Portal.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class CA_TASK_VW
    {
        public decimal CA_TASK_ID { get; set; }
        public decimal TASK_ID { get; set; }
        public string TASK_CODE { get; set; }
        public string TASK_DESC { get; set; }
        public Nullable<decimal> TASK_SEQ { get; set; }
        public string CAL_WEEK_DAY_FLAG { get; set; }
        public decimal DEFAULT_DURATION_DAYS { get; set; }
        public Nullable<decimal> RESPONSIBLE_DEPARTMENT_ID { get; set; }
        public Nullable<decimal> RESPONSIBLE_DIVISION_ID { get; set; }
        public Nullable<decimal> RESPONSIBLE_USER_ID { get; set; }
        public decimal START_POINT { get; set; }
        public decimal END_POINT { get; set; }
        public Nullable<System.DateTime> TARGET_START_DATE { get; set; }
        public Nullable<System.DateTime> TARGET_END_DATE { get; set; }
        public Nullable<System.DateTime> ACTUAL_START_DATE { get; set; }
        public Nullable<System.DateTime> ACTUAL_END_DATE { get; set; }
        public string TASK_AVAILABLE_FLAG { get; set; }
        public string TASK_COMPLETE_FLAG { get; set; }
        public decimal RESULT_SET_ID { get; set; }
        public string RESULT_CODE { get; set; }
        public Nullable<System.DateTime> TASK_COMPLETE_DATE { get; set; }
        public Nullable<decimal> TASK_COMPLETE_BY { get; set; }
        public string TASK_TYPE { get; set; }
        public Nullable<decimal> HEARING_TYPE_ID { get; set; }
        public Nullable<decimal> DISCIPLINE_ID { get; set; }
        public Nullable<decimal> LEAD_DAYS { get; set; }
        public Nullable<decimal> RESCHEDULE_AFTER_DAYS { get; set; }
        public Nullable<decimal> WORK_UNITS { get; set; }
        public Nullable<decimal> CORR_GROUP_ID { get; set; }
        public Nullable<decimal> TIME_CUT { get; set; }
        public string AUTO_SCHEDULE_INSP_FLAG { get; set; }
        public Nullable<decimal> AUTO_SCHEDULE_INSP_DAYS { get; set; }
        public string TASK_GEO_FLAG { get; set; }
        public Nullable<decimal> GEO_AREA_ID { get; set; }
        public Nullable<decimal> GEO_DETAIL_ID { get; set; }
        public string EXPIRED_FLAG { get; set; }
        public Nullable<System.DateTime> DATE_EXPIRED { get; set; }
        public decimal CREATED_BY { get; set; }
        public System.DateTime DATE_CREATED { get; set; }
        public Nullable<decimal> MODIFIED_BY { get; set; }
        public Nullable<System.DateTime> DATE_MODIFIED { get; set; }
        public string REGISTERED_FLAG { get; set; }
        public string ANONYMOUS_FLAG { get; set; }
        public string TABLE_NAME { get; set; }
        public string DEPARTMENT_CODE { get; set; }
        public string DEPARTMENT_NAME { get; set; }
        public string DISCIPLINE_CODE { get; set; }
        public string DISCIPLINE_DESC { get; set; }
        public string LOGIN_ID { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string RESULT_SET_DESC { get; set; }
        public string HEARING_TYPE { get; set; }
        public string HEARING_DESC { get; set; }
        public string CORR_GROUP_CODE { get; set; }
        public string CORR_GROUP_DESC { get; set; }
        public string GEO_AREA { get; set; }
        public string GEO_AREA_DESC { get; set; }
        public string GEO_DETAIL { get; set; }
        public string GEO_DETAIL_DESC { get; set; }
        public string DIVISION_CODE { get; set; }
        public string DIVISION_NAME { get; set; }
        public decimal ORG_ID { get; set; }
        public string TASK_COMPLETED_BY_LOGIN_ID { get; set; }
        public string TASK_COMPLETED_BY_FIRST_NAME { get; set; }
        public string TASK_COMPLETED_BY_LAST_NAME { get; set; }
        public Nullable<System.DateTime> TASK_AVAILABLE_DATE { get; set; }
        public string CREATED_BY_LOGIN_ID { get; set; }
        public string MODIFIED_BY_LOGIN_ID { get; set; }
        public Nullable<decimal> CONFIRMATION_ID { get; set; }
        public decimal CA_OBJECT_ID { get; set; }
        public decimal CASE_TYPE_ID { get; set; }
        public string CASE_TYPE { get; set; }
        public string CASE_TYPE_DESC { get; set; }
        public Nullable<decimal> SUB_TYPE_ID { get; set; }
        public string SUB_TYPE { get; set; }
        public string SUB_TYPE_DESC { get; set; }
        public string CASE_NUMBER { get; set; }
        public string CASE_NAME { get; set; }
        public Nullable<decimal> PROJECT_ID { get; set; }
        public string PROJECT_CODE { get; set; }
        public string PROJECT_DESC { get; set; }
        public string CASE_STATUS { get; set; }
        public string LOCATION { get; set; }
    }
}
