//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Portal.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class CA_TASK_REL_DOCS
    {
        public decimal CA_TASK_REL_DOC_ID { get; set; }
        public decimal CA_OBJECT_ID { get; set; }
        public decimal CA_TASK_ID { get; set; }
        public decimal CA_REL_DOC_ID { get; set; }
        public decimal CREATED_BY { get; set; }
        public System.DateTime DATE_CREATED { get; set; }
        public Nullable<decimal> MODIFIED_BY { get; set; }
        public Nullable<System.DateTime> DATE_MODIFIED { get; set; }
    
        public virtual CA_OBJECT CA_OBJECT { get; set; }
        public virtual CA_REL_DOCS CA_REL_DOCS { get; set; }
        public virtual CA_TASK CA_TASK { get; set; }
    }
}
