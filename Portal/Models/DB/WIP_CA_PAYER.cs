//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Portal.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class WIP_CA_PAYER
    {
        public decimal CA_PAYER_ID { get; set; }
        public Nullable<decimal> CA_RECEIPT_ID { get; set; }
        public string NAME { get; set; }
        public string ADDRESS_LINE1 { get; set; }
        public string ADDRESS_LINE2 { get; set; }
        public string ADDRESS_LINE3 { get; set; }
        public string CITY_NAME { get; set; }
        public string STATE_CODE { get; set; }
        public string COUNTRY_CODE { get; set; }
        public string ZIP_CODE { get; set; }
        public string PHONE_WORK { get; set; }
        public string PHONE_WORK_EXT { get; set; }
        public string PHONE_HOME { get; set; }
        public string PHONE_MOBILE { get; set; }
        public string FAX_NUMBER { get; set; }
        public string EMAIL { get; set; }
        public string WEB_SITE_URL { get; set; }
        public string COMMENT_TEXT { get; set; }
        public Nullable<decimal> CREATED_BY { get; set; }
        public Nullable<System.DateTime> DATE_CREATED { get; set; }
        public Nullable<decimal> MODIFIED_BY { get; set; }
        public Nullable<System.DateTime> DATE_MODIFIED { get; set; }
    
        public virtual WIP_CA_RECEIPTS WIP_CA_RECEIPTS { get; set; }
    }
}
