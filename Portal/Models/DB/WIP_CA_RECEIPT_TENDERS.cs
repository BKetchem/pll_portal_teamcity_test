//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Portal.Models.DB
{
    using System;
    using System.Collections.Generic;
    
    public partial class WIP_CA_RECEIPT_TENDERS
    {
        public decimal CA_RECEIPT_TENDERS_ID { get; set; }
        public Nullable<decimal> CA_RECEIPT_ID { get; set; }
        public Nullable<decimal> TENDER_TYPE_ID { get; set; }
        public string TENDER_TYPE_CODE { get; set; }
        public string TENDER_TYPE_DESC { get; set; }
        public Nullable<decimal> TENDER_AMOUNT { get; set; }
        public Nullable<decimal> CHANGE_AMOUNT { get; set; }
    
        public virtual WIP_CA_RECEIPTS WIP_CA_RECEIPTS { get; set; }
    }
}
