﻿using Portal.Models.DB;
using System.ComponentModel.DataAnnotations;

namespace Portal.Models.ViewModel
{
    public class CreateUserModel
    {
        public PAA_EXTERNAL_USERS ExternalUser { get; set; }

        [DataType(DataType.Password)]
        public string Password { get; set; }
     

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Passwords do not match")]
        public string ConfirmPassword { get; set; }
    }
}