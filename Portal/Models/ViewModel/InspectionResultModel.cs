﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ViewModel
{
    public class InspectionResultModel
    {
        public long? ConfirmationId { get; set; }
        public string PermitNumber { get; set; }
        public string Comments { get; set; }
        public DateTime InspectionDate { get; set; }
        public string InspectionTypeDesc { get; set; }
    }
}