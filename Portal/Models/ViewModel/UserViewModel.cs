﻿using Portal.Models.DB;

namespace Portal.Models.ViewModel
{
    public class UserViewModel
    {
        public USER User { get; set; }
       
        public ModifyAccount ModifyAccount { get; set; }

        public CONTRACTOR_VW Contractor { get; set; }

    }
}