﻿using Portal.Models.DB;
using System;
using System.Collections.Generic;

namespace Portal.Models.ViewModel
{
    public class InspectionRequestModel
    {
        public List<Inspection_Requests> Inspection_Requests = new List<Inspection_Requests>();

        public List<CA_DATA_DETAIL_VW> CaseDataDetails = new List<CA_DATA_DETAIL_VW>();
        public string Time_Preference { get; set; }

        public string Comments { get; set; }

        public AvailableTimes Times { get; set; }

        public DateTime SelectedDay { get; set; }
        public string CaseNumber { get; set; }

        public decimal CaObjectId { get; set; }

        public decimal CaTaskId { get; set; }

        public string ScheduledDate { get; set; }

        public string InspectionType { get; set; }

        public string TaskCode { get; set; }

        public string TaskDesc { get; set; }
    }

    public class AvailableTimes
    {
        public string End { get; set; }
        public string Start { get; set; }
    }

    public class Inspection_Requests
    {
        public string CASE_NUMBER { get; set; }
        public string CASE_TYPE { get; set; }
        public decimal CA_OBJECT_ID { get; set; }
        public string TASK_CODE { get; set; }
        public string TASK_DESC { get; set; }
        public string LOCATION { get; set; }
        public Nullable<decimal> USER_ID { get; set; }
        public decimal CA_TASK_ID { get; set; }
        public string SUB_TYPE { get; set; }
    }

}