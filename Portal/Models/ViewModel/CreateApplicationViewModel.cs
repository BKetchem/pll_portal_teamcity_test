﻿using Cityworks.Core;
using Portal.Models.DB;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Portal.Models.ViewModel
{
    public class CreateApplicationViewModel
    {
        public string ChildOrStandAlone { get; set; }

        public List<Address> Addresses = new List<Address>();

        public string Name { get; set; }

        public List<CONTRACTOR> Contractor = new List<CONTRACTOR>();

        public List<CA_OBJECT_VW> ParentCase = new List<CA_OBJECT_VW>();

        public List<DataDetails> DataDetailsList = new List<DataDetails>();

        public CaseType CaseType = new CaseType();

        public CaseContractor CaseContractor = new CaseContractor();

        public ContractorLicense ContractorLicense = new ContractorLicense();

        public CA_OBJECT CaObject { get; set; }

        public List<CaFlagsItemBase> CaseFlags = new List<CaFlagsItemBase>();

        public CasePeople People = new CasePeople();

        public CustomPerson Person { get; set; }

        public decimal WipCaObjectId { get; set; }

    }

    public class CasePeople
    {
        public List<Person> PeopleList = new List<Person>();
        public List<PEOPLE_ROLE> Roles = new List<PEOPLE_ROLE>();
        // public IEnumerable<SelectListItem> RolesTest = new SelectListItem[] { };
        //public string SelectedRole { get; set; }
    }

    public class CustomPerson
    {
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string RoleId { get; set; }
    }

    public class CustomContractor
    {
        public decimal ContractorId { get; set; }
        public long ContractorTypeId { get; set; }
        public long StateLicenseId { get; set; }
        public string ContractorName { get; set; }
        public string ContractorType{ get; set; }
        public string License_Num { get; set; }
    }

    public class Address
    {
        public long? AddressId { get; set; }
        public long? AssetId { get; set; }
        public string AssetType { get; set; }
        public string CityName { get; set; }
        public long? FeatureAssetId { get; set; }
        public string FeatureClass { get; set; }
        public long? FeatureObjectId { get; set; }
        public string Location { get; set; }

        public string StateCode { get; set; }
        public string StreetDirection { get; set; }
        public string StreetFraction { get; set; }
        public string StreetName { get; set; }
        public long? StreetNumber { get; set; }
        public string StreetType { get; set; }
        public string Suite { get; set; }

        public decimal? XCoord { get; set; }
        public decimal? YCoord { get; set; }

        public long? ZipCode { get; set; }
    }

    public class CaseContractor
    {
        public CONTRACTOR ContractorEntity { get; set; }

        public List<ContractorList> ContractorList = new List<ContractorList>();

        public STATE_LICENSE_VW ContractorLicense { get; set; }

        public string SearchValue { get; set; }
    }

    public class ContractorList
    {
        public decimal CONTRACTOR_ID { get; set; }
        public decimal ORG_ID { get; set; }
        public Nullable<decimal> USER_ID { get; set; }
        public string BUSINESS_NAME { get; set; }
        public string FIRST_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string ADDRESS_LINE1 { get; set; }
        public string ADDRESS_LINE2 { get; set; }
        public string ADDRESS_LINE3 { get; set; }
        public string CITY_NAME { get; set; }
        public string STATE_CODE { get; set; }
        public string COUNTRY_CODE { get; set; }
        public string ZIP_CODE { get; set; }
        public string PHONE_WORK { get; set; }
        public string PHONE_WORK_EXT { get; set; }
        public string PHONE_HOME { get; set; }
        public string PHONE_MOBILE { get; set; }
        public string FAX_NUMBER { get; set; }
        public string EMAIL { get; set; }
        public string WC_LIABILITY_COMPANY { get; set; }
        public Nullable<System.DateTime> WC_LIABILITY_EXPIRE_DATE { get; set; }
        public string COMMENT_TEXT { get; set; }
        public string HOLD_FLAG { get; set; }
        public string EXPIRED_FLAG { get; set; }
        public Nullable<System.DateTime> DATE_EXPIRED { get; set; }
        public decimal CREATED_BY { get; set; }
        public System.DateTime DATE_CREATED { get; set; }
        public Nullable<decimal> MODIFIED_BY { get; set; }
        public Nullable<System.DateTime> DATE_MODIFIED { get; set; }
        public string GEN_LIABILITY { get; set; }
        public Nullable<System.DateTime> GEN_LIABILITY_EXP_DATE { get; set; }
        public decimal ContractorId { get; set; }
        public long ContractorTypeId { get; set; }
        public string ContractorTypeDesc { get; set; }
        public string License_Num { get; set; }
        public long StateLicenseId { get; set; }
        public System.DateTime LICENSE_EXPIRATION_DATE { get; set; }
    }

    public class ContractorLicense
    {
        public decimal ContractorId { get; set; }
        public long ContractorTypeId { get; set; }
        public long LocalLicenseId { get; set; }
        public long StateLicenseId { get; set; }
    }

    public class Parent
    {
        public string ParentCaseNumber { get; set; }
        public string ParentLocation { get; set; }
        public decimal? ParentCX { get; set; }
        public decimal? ParentCY { get; set; }
        public string ParentCaseType { get; set; }
        public string ParentCaseGroup { get; set; }
    }

    public class CaseType
    {
        public List<string> CaseGroups = new List<string>();
        public IEnumerable<SelectListItem> SubTypes = new SelectListItem[] { };
        public IEnumerable<SelectListItem> CaseTypes { get; set; }
        public long CaseTypeId { get; set; }
        public string SelectedCaseGroup { get; set; }
        public string SelectedCaseType { get; set; }
        public long SelectedCaseTypeId { get; set; }
        public string SelectedSubType { get; set; }
        public long? SubTypeId { get; set; }

        //public string CaseTypeDesc { get; set; }

        //public string CaseTypeCode { get; set; }
    }

    public class DataDetails
    {
        public string GroupCode { get; set; }
        public string GroupDesc { get; set; }
        public decimal DetailSequence { get; set; }
        public string DetailCode { get; set; }
        public string DetailDesc { get; set; }
        public string YesNoFlag { get; set; }
        public string NumberFlag { get; set; }
        public string ValueFlag { get; set; }
        public string DateFlag { get; set; }
        public string ListValuesFlag { get; set; }
        public string CommentFlag { get; set; }
        public string ColumnSequence { get; set; }
        public string TextFlag { get; set; }
        public string CheckboxFlag { get; set; }
        public string YesNoValue { get; set; }
        public decimal NumberValue { get; set; }
        public DateTime? DateValue { get; set; }
        public string ListValue { get; set; }
        public string CommentValue { get; set; }
        public string TextValue { get; set; }
        public bool CheckboxValue { get; set; }
        public decimal Quantity { get; set; }
    }

    public class Persons
    {
        public decimal? PEOPLE_ID { get; set; }
        public decimal? ORG_ID { get; set; }
        public Nullable<decimal> ROLE_ID { get; set; }
        public string NAME { get; set; }
        public string ADDRESS_LINE1 { get; set; }
        public string ADDRESS_LINE2 { get; set; }
        public string ADDRESS_LINE3 { get; set; }
        public string CITY_NAME { get; set; }
        public string STATE_CODE { get; set; }
        public string COUNTRY_CODE { get; set; }
        public string ZIP_CODE { get; set; }
        public string PHONE_WORK { get; set; }
        public string PHONE_WORK_EXT { get; set; }
        public string PHONE_HOME { get; set; }
        public string PHONE_MOBILE { get; set; }
        public string FAX_NUMBER { get; set; }
        public string EMAIL { get; set; }
        public string WEB_SITE_URL { get; set; }
        public string COMMENT_TEXT { get; set; }
        public Nullable<decimal> USER_ID { get; set; }
        public string TABLE_NAME { get; set; }
        public string EXPIRED_FLAG { get; set; }
        public Nullable<System.DateTime> DATE_EXPIRED { get; set; }
        public decimal CREATED_BY { get; set; }
        public System.DateTime DATE_CREATED { get; set; }
        public Nullable<decimal> MODIFIED_BY { get; set; }
        public Nullable<System.DateTime> DATE_MODIFIED { get; set; }
    }
}