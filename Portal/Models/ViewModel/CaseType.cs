﻿using Cityworks.Core;
using Portal.Models.DB;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Portal.Models.ViewModel
{
    public class CaseTypes
    {
        // public string CaseType { get; set; }
        public List<string> CaseGroups = new List<string>();

        public IEnumerable<SelectListItem> SubTypeList = new SelectListItem[] { };
        public IEnumerable<SelectListItem> CaseTypeList { get; set; }
        public long CaseTypeId { get; set; }
        public string SelectedCaseGroup { get; set; }
        public string SelectedCaseType { get; set; }
        public long SelectedCaseTypeId { get; set; }
        public string SelectedSubType { get; set; }
        public long? SubTypeId { get; set; }

        public List<CA_OBJECT_VW> CaObject = new List<CA_OBJECT_VW>();

    }
}