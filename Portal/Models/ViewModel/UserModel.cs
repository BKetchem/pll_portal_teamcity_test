﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Portal.Models.ViewModel
{
    public class UserModel
    {
    }

    public class UserLoginView
    {
        [Key]
        public decimal USER_ID { get; set; }
        [Required(ErrorMessage = "*")]
        [Display(Name = "Login ID")]
        public string LoginName { get; set; }
        [Required(ErrorMessage = "*")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        public string Role { get; set; }
        [HiddenInput]
        public string ReturnUrl { get; set; }
    }
}