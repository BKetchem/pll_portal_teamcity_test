﻿using Portal.Models.DB;
using System;
using System.Linq;

namespace Portal.Models.ViewModel
{
    /// <summary>
    /// Contains code to save create application WIP
    /// </summary>
    public class WIPViewModel
    {
        private readonly CityworksEntities _db = new CityworksEntities();

        /// <summary>
        /// Creates the initial WIP db record
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public CreateApplicationViewModel WIPInsert(CreateApplicationViewModel model)
        {
            WIP_CA_OBJECT wip = new WIP_CA_OBJECT
            {
                CA_OBJECT_ID = (from i in _db.PWSYSIDs
                                where i.SYSTYPE.Equals("WIP_CA_OBJECT")
                                select i.ID).FirstOrDefault(),
                CASE_NAME = model.CaObject.CASE_NAME,
                EXPIRED_FLAG = "N" //required field
            };
            _db.WIP_CA_OBJECT.Add(wip);
            _db.SaveChanges();
            model.WipCaObjectId = wip.CA_OBJECT_ID;
            UpdatePwSysId("WIP_CA_OBJECT");
            return model;
        }

        /// <summary>
        /// Updates the Cityworks PWSYSID table.
        /// Cityworks uses this for managing database Ids.
        /// </summary>
        /// <param name="table"></param>
        private void UpdatePwSysId(string table)
        {
            var pwSysId = (from i in _db.PWSYSIDs
                           where i.SYSTYPE.Equals(table)
                           select i).FirstOrDefault();
            if (pwSysId == null) throw new ArgumentNullException(nameof(pwSysId));
            pwSysId.ID = (pwSysId.ID + 1);
            try
            {
                _db.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        /// <summary>
        /// Sets case and sub type IDs
        /// </summary>
        /// <param name="model"></param>
        public void WIPCaseType(CreateApplicationViewModel model)
        {
            var wipObject = (from i in _db.WIP_CA_OBJECT
                             where i.CA_OBJECT_ID.Equals(model.WipCaObjectId)
                             select i).FirstOrDefault();
            if (wipObject != null)
            {
                wipObject.CASE_TYPE_ID = model.CaseType.CaseTypeId;
                wipObject.SUB_TYPE_ID = model.CaseType.SubTypeId;
            }
            _db.SaveChanges();
        }

        /// <summary>
        /// Inserts WIP case addresses
        /// </summary>
        /// <param name="model"></param>
        public void WIPAddresses(CreateApplicationViewModel model)
        {
            var wipObject = (from i in _db.WIP_CA_OBJECT
                             where i.CA_OBJECT_ID.Equals(model.WipCaObjectId)
                             select i).FirstOrDefault();
            if (wipObject != null)
            {
                wipObject.LOCATION = model.Addresses[0].Location;
                wipObject.CX = model.Addresses[0].XCoord;
                wipObject.CY = model.Addresses[0].YCoord;
            }
            foreach (var addr in model.Addresses)
            {
                WIP_CA_ADDRESS wip = new WIP_CA_ADDRESS
                {
                    CA_ADDRESS_ID = (from i in _db.PWSYSIDs
                                     where i.SYSTYPE.Equals("WIP_CA_ADDRESS")
                                     select i.ID).FirstOrDefault(),
                    CA_OBJECT_ID = model.WipCaObjectId,
                    ADDRESS_ID = addr.AddressId,
                    STREET_NUMBER = addr.StreetNumber,
                    STREET_DIRECTION = addr.StreetDirection,
                    STREET_FRACTION = addr.StreetFraction,
                    STREET_NAME = addr.StreetName,
                    STREET_TYPE = addr.StreetType,
                    STREET_POSTDIR = addr.StreetDirection,
                    SUITE = addr.Suite,
                    CITY_NAME = addr.CityName,
                    STATE_CODE = addr.StateCode,
                    ZIP_CODE = addr.ZipCode.ToString(),
                    ASSET_ID = addr.AssetId.ToString(),
                    ASSET_TYPE = addr.AssetType,
                    FEATURE_OBJECT_ID = addr.FeatureObjectId,
                    FEATURE_CLASS = addr.FeatureClass,
                    LOCATION = addr.Location,
                    X_COORD = addr.XCoord,
                    Y_COORD = addr.YCoord
                };
                UpdatePwSysId("WIP_CA_ADDRESS");
                _db.WIP_CA_ADDRESS.Add(wip);
            }
            _db.SaveChanges();
        }

        /// <summary>
        /// Insert WIP case flags.
        /// UpdatePwSysId is not needed here as this is a custom table
        /// and the table has identity specification.
        /// </summary>
        /// <param name="model"></param>
        public void AddFlags(CreateApplicationViewModel model)
        {
            foreach (var flag in model.CaseFlags)
            {
                WIP_CA_FLAGS flags = new WIP_CA_FLAGS
                {
                    CA_OBJECT_ID = model.WipCaObjectId,
                    FLAG_ID = Convert.ToDecimal(flag.FlagId)
                };
                _db.WIP_CA_FLAGS.Add(flags);
            }
            _db.SaveChanges();
        }

        /// <summary>
        /// Insert WIP people
        /// </summary>
        /// <param name="model"></param>
        public void AddPeople(CreateApplicationViewModel model)
        {
            foreach (var person in model.People.PeopleList)
            {
                WIP_CA_PEOPLE wipPerson = new WIP_CA_PEOPLE
                {
                    CA_PEOPLE_ID = (from i in _db.PWSYSIDs
                                    where i.SYSTYPE.Equals("WIP_CA_PEOPLE")
                                    select i.ID).FirstOrDefault(),
                    CA_OBJECT_ID = model.WipCaObjectId,
                    PEOPLE_ID = person.PEOPLE_ID,
                    ROLE_ID = person.ROLE_ID,
                    NAME = person.NAME,
                    ADDRESS_LINE1 = person.ADDRESS_LINE1,
                    ADDRESS_LINE2 = person.ADDRESS_LINE2,
                    ADDRESS_LINE3 = person.ADDRESS_LINE3,
                    CITY_NAME = person.CITY_NAME,
                    STATE_CODE = person.STATE_CODE,
                    COUNTRY_CODE = person.COUNTRY_CODE,
                    ZIP_CODE = person.ZIP_CODE,
                    PHONE_WORK = person.PHONE_WORK,
                    PHONE_HOME = person.PHONE_HOME,
                    PHONE_MOBILE = person.PHONE_MOBILE,
                    FAX_NUMBER = person.FAX_NUMBER,
                    EMAIL = person.EMAIL,
                    WEB_SITE_URL = person.WEB_SITE_URL,
                    COMMENT_TEXT = person.COMMENT_TEXT,
                    CREATED_BY = person.CREATED_BY,
                    MODIFIED_BY = person.MODIFIED_BY,
                    PHONE_WORK_EXT = person.PHONE_WORK_EXT,
                    COMPANY_NAME = person.COMPANY_NAME
                };
                UpdatePwSysId("WIP_CA_PEOPLE");
                _db.WIP_CA_PEOPLE.Add(wipPerson);
            }
            _db.SaveChanges();
        }

        public void AddDataDetails(CreateApplicationViewModel model)
        {
            foreach (var detail in model.DataDetailsList)
            {
                WIP_CA_DATA_DETAIL wipDetail = new WIP_CA_DATA_DETAIL
                {
                    CA_DATA_DETAIL_ID = (from i in _db.PWSYSIDs
                                         where i.SYSTYPE.Equals("WIP_CA_DATA_DETAIL")
                                         select i.ID).FirstOrDefault(),
                    
                };
            }
        }
    }
}