﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Portal.Models.ViewModel
{
    public class InspectionModel
    {
        public string Inspection_Type { get; set; }
        public string Inspection_Desc { get; set; }
        public string Permit_Number { get; set; }
        public string Permit_Type { get; set; }
        public string Permit_Desc { get; set; }
        public string Location { get; set; }
        public int Permit_Id { get; set; }
        public int Task_Id { get; set; }
    }
}