﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Portal.Models.DB;
using System.Text;
using System.Security.Cryptography;

namespace Portal.Models.EntityManager
{
    public class UserManager
    {
        public bool IsLoginNameExist(string loginName)
        {
            using (CityworksEntities db = new CityworksEntities())
            {
                return db.USERS.Where(o => o.LOGIN_ID.Equals(loginName)).Any();
            }
        }

        public string GetUserPassword(string loginName)
        {
            using (CityworksEntities db = new CityworksEntities())
            {
                var user = db.USERS.Where(o => o.LOGIN_ID.ToLower().Equals(loginName));
                if (user.Any())
                    return user.FirstOrDefault().PASSWORD;
                else
                    return string.Empty;
            }
        }

        public decimal GetUserId(string loginName)
        {
            using (CityworksEntities db = new CityworksEntities())
            {
                var user = db.USERS.Where(o => o.LOGIN_ID.ToLower().Equals(loginName));
                if (user.Any())
                    return user.FirstOrDefault().USER_ID;
                else
                    return 0;
            }
        }

        public string GetMD5Hash(string password)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(password);
            byte[] hash = md5.ComputeHash(inputBytes);

            // convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public string GetUserRole(decimal user_id)
        {
            using (CityworksEntities db = new CityworksEntities())
            {
                decimal role_id = 0;
                var q = db.USER_ROLES_VW.Where(o => o.USER_ID.Equals(user_id));
                if (q.Any())
                {
                    role_id = q.FirstOrDefault().ROLE_ID;
                    var _q = db.SECURITY_ROLES.Where(o => o.ROLE_ID.Equals(role_id));
                    return _q.FirstOrDefault().ROLE_CODE;
                }
                    
                else
                    return "GUEST";
            }
        }
    }
}