﻿using Portal.Models.DB;
using System.Linq;

namespace Portal.Models.EntityManager
{
    public class ReviewApplicationsManager
    {
        public string GetOpenApplications(string loginName)
        {
            using (CityworksEntities db = new CityworksEntities())
            {
                //  var app = db.CA_OBJECT_VW.Where(o => o.CREATED_BY_LOGIN_ID.ToLower().Equals(loginName))
                var q = from o in db.CA_OBJECT_VW
                        select new
                        {
                            Case_Number = o.CASE_NUMBER,
                            Case_Status = o.CASE_STATUS,
                            Location = o.LOCATION,
                            Date_Created = o.DATE_CREATED
                        };

                var result = q.ToList();
                return result.ToString();
            }
        }
    }
}
