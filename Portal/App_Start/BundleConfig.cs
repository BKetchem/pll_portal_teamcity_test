﻿using System.Web.Optimization;

namespace Portal
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            
            bundles.Add(new StyleBundle("~/bundles/css").Include(
                      "~/Content/roboto.css",
                      "~/Content/material-icons.css",
                      "~/Content/bootstrap/dist/css/bootstrap.min.css",
                      "~/Content/BootstrapDatePicker/css/bootstrap-datepicker3.min.css",
                      "~/Content/bootstrap-material/bootstrap-material-design.css",
                      "~/Content/bootstrap-material/ripples.css",
                      "~/Content/DataTables/datatables.min.css",
                      "~/Content/DataTables/Responsive-2.1.0/css/responsive.dataTables.min.css",
                      "~/Content/DataTables/Select-1.2.0/css/select.dataTables.min.css",
                      "~/Content/main.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/bootstrap-material/jquery.dropdown.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/javascript").Include(
                      "~/Scripts/jquery-{version}.js",
                      "~/Scripts/jquery-ui-1.11.4.min.js",
                      "~/Scripts/jquery.unobtrusive-ajax.min.js",
                      "~/Scripts/jquery.validate.js",
                      "~/Scripts/jquery.validate.unobtrusive.min.js",
                      "~/Content/bootstrap/dist/js/bootstrap.min.js",
                      "~/scripts/bootstrap-material/material.js",
                      "~/scripts/bootstrap-material/ripples.js",
                      "~/Content/BootstrapDatePicker/js/bootstrap-datepicker.min.js",
                      "~/Scripts/jsConfig.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/dataTables").Include(
                    "~/Content/DataTables/datatables.min.js",
                    "~/Content/DataTables/Responsive-2.1.0/js/dataTables.responsive.min.js",
                    "~/Content/DataTables/Select-1.2.0/js/dataTables.select.min.js"

                ));
        }
    }
}