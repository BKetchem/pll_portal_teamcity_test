﻿var map;

require([
  "esri/config",
  "esri/graphic",
  "esri/geometry/Point",
  "esri/InfoTemplate",
  "esri/map",
  "esri/dijit/Popup",
  "esri/dijit/PopupTemplate",
  "esri/dijit/Search",
  "esri/geometry/Extent",
  "esri/layers/FeatureLayer",
  "esri/layers/GraphicsLayer",
  "esri/layers/ArcGISDynamicMapServiceLayer",
  "esri/symbols/SimpleFillSymbol",
  "esri/symbols/SimpleLineSymbol",
  "esri/symbols/SimpleMarkerSymbol",
  'esri/symbols/PictureMarkerSymbol',
  "esri/tasks/find",
  "esri/tasks/GeometryService",
  "esri/tasks/query",
  "esri/tasks/QueryTask",
  "dojo/dom",
  "dojo/dom-construct",
  "dojo/dom-class",
  "dojo/on",
  "dojo/parser",
  "esri/Color",
  "dojo/_base/lang",
  "dojo/date/locale",
   "dojo/_base/fx",
   "dojo/promise/all",
   "esri/geometry/geometryEngine",
  "dojo/domReady!"
],
  function (
    esriConfig, Graphic, Point, InfoTemplate, Map, Popup, PopupTemplate, Search, Extent, FeatureLayer,
    GraphicsLayer, ArcGISDynamicMapServiceLayer, SimpleFillSymbol, SimpleLineSymbol, SimpleMarkerSymbol,
    PictureMarkerSymbol,
    find, GeometryService, Query, QueryTask, dom, domConstruct, domClass, on, parser, Color, lang, locale, fx, all, geometryEngine
  ) {
      parser.parse();
      //All Map configuration details to be entered here..
      var mapConfig = {
          extent: { "xmin": -9382513.8241863, "ymin": 3559906.181829513, "xmax": -9381461.620718902, "ymax": 3560503.3461129405, "spatialReference": { "wkid": 102100 } },
          ParcelMpSVcURL: "http://tlcgisinter.leoncountyfl.gov/arcgis/rest/services/Public/COT_Inter_Cityworks_PLL_D_WM/MapServer",
          SiteAddressFlyr: {
              URL: "http://tlcgisinter.leoncountyfl.gov/arcgis/rest/services/Public/COT_Inter_Cityworks_PLL_D_WM/MapServer/0",
              OutFields: ["*"],
              AddressField: "SITEADDR",
              PINField: ["PIN"],
              BlockNbr : ["MVPARCEL"],
              InfoTemplateFields:
              {
                  "Address": "SITEADDR",
                  "X Coordinate:": "POINT_X",
                  "Y Coordinate": "POINT_Y"
              }
          },
          EsriLocatorAddressField: "StAddr",
          ParcelPropInfoFLyr: {
              URL: "http://tlcgisinter.leoncountyfl.gov/arcgis/rest/services/Public/TLC_Inter_PropertyInfo_D_WM/MapServer/2",
              OutFields: ["FLDZONE", "CITYINOUT"],
              FloodZoneProperty: "FLDZONE",
              NoFloodZoneValues: ["None"],
              CityZoneProperty: "CITYINOUT",
              CityInValues: ["IN"]
          },
          MMTDFLyrURL: "http://tlcgisinter.leoncountyfl.gov/arcgis/rest/services/Public/TLC_Inter_OverlaySpecialPlanningAreas_WM_D/MapServer/0",
          HPOFLyrURL: "http://tlcgisinter.leoncountyfl.gov/arcgis/rest/services/Public/TLC_Inter_OverlaySpecialPlanningAreas_WM_D/MapServer//1"
      };


      var ext = new esri.geometry.Extent(mapConfig.extent);
      var selectionExtent = new Object;
      var selectionObjectIDs = new Array;

      var popup = new Popup({
          fillSymbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
            new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
              new Color([255, 0, 0]), 2), new Color([255, 255, 0, 0.25]))
      }, domConstruct.create("div"));

      domClass.add(popup.domNode, "myTheme");

      var map = new Map("mapDiv", {
          basemap: "topo",
          extent: ext,
          infoWindow: popup
      });

      var parcelPropInfoURL = mapConfig.ParcelPropInfoFLyr.URL;
      var MMTDFLyrURL = mapConfig.MMTDFLyrURL;
      var HPOLyrURL = mapConfig.HPOFLyrURL;

      var parcelPropInfoQueryTask = new QueryTask(parcelPropInfoURL);
      var MMTDQueryTask = new QueryTask(MMTDFLyrURL);
      var HPOQueryTask = new QueryTask(HPOLyrURL)

      var serviceLayer = new ArcGISDynamicMapServiceLayer(mapConfig.ParcelMpSVcURL);
      map.addLayer(serviceLayer);

      // Template for the popup window.
      // Shows all attributes of the feature.
      var infoTemplateContentTemplate = "";
      var infoTemplateFields = mapConfig.SiteAddressFlyr.InfoTemplateFields;
      for (var key in infoTemplateFields) {         
          infoTemplateContentTemplate += "<b>" + key + ":</b> ${" + infoTemplateFields[key] + "}<br/>";          
      }
      var template = new InfoTemplate("Attributes", infoTemplateContentTemplate);

      // Add the points from the PLL map service as FeatureLayer.
      var SiteAddressFlyr = new FeatureLayer(mapConfig.SiteAddressFlyr.URL, {
          mode: FeatureLayer.MODE_ONDEMAND,
          outFields: mapConfig.SiteAddressFlyr.OutFields
          //infoTemplate: template
      });

      map.addLayer(SiteAddressFlyr);
      var gl = new GraphicsLayer();
      map.addLayer(gl);
      // Gets tolerance distance for the map click event.
      function pointToExtent(map, point, toleranceInPixel) {
          var pixelWidth = map.extent.getWidth() / map.width;
          var toleranceInMapCoords = toleranceInPixel * pixelWidth;
          return new Extent(point.x - toleranceInMapCoords,
                            point.y - toleranceInMapCoords,
                            point.x + toleranceInMapCoords,
                            point.y + toleranceInMapCoords,
                            map.spatialReference);
      }

      symbol = new esri.symbol.SimpleMarkerSymbol();
      symbol.setStyle(esri.symbol.SimpleMarkerSymbol.STYLE_DIAMOND);
      symbol.setSize(20);
      symbol.setColor(new dojo.Color([255, 255, 0, 0.5]));


      var identifySymbol = new PictureMarkerSymbol({
          "angle": 0,
          "xoffset": 0,
          "yoffset": 5,
          "type": "esriPMS",
          "url": "http://img3.wikia.nocookie.net/__cb20140427224234/caramelangel714/images/7/72/Location_Icon.png",
          "contentType": "image/png",
          "width": 18,
          "height": 27
      });

      // Search task on CaObjecId
      var pinSearch = new Search({
          map: map,
          sources: [],
          zoomScale: 1000
      }, "");

      pinSearch.on("load", function () {
          var sources = pinSearch.sources;
          sources.push({
              featureLayer: SiteAddressFlyr,
              searchFields: mapConfig.SiteAddressFlyr.PINField,
              outFields: ["*"],
              infoTemplate: template
          });
          pinSearch.set("sources", sources);
      });
      pinSearch.startup();

      pinSearch.on("select-result", function (event) {
          populateWithFeatureInfo(event.result.feature);
      });

      function doFind() {
          pinSearch.search(pin);
      }
      // When table input in CreateApplication.aspx is clicked
      // fire this event to get the CaObjectId(PermitId) and perform a Search on that value.
      var pin;
      $(document).on("click", 'input[name=createApp]', function (event) {
          var selectedRow = $('input[name=createApp]:checked').closest('tr');
          pin = $(this).closest('tr').children('td:nth-child(8)').text();
          doFind();
      });
      map.on("extent-change", function (event) {
          console.log(event.Extent);
      })
      // Click event to show feature popup.
      SiteAddressFlyr.on("click", function (event) {
          SiteAddressFlyr.clearSelection();
          var feature = event.graphic
          if (feature) {
              populateWithFeatureInfo(feature);             
          }
      });

      gl.on("click", function (event) {
          event.preventDefault();
          gl.remove(event.graphic);
          refreshList();
      });

      gl.on("graphic-add", function (graphic) {
          refreshList();
      });

      gl.on("mouse-over", function (event) {
          var g = event.graphic;
          map.infoWindow.setContent(g.getContent());
          map.infoWindow.setTitle(g.getTitle());
          map.infoWindow.show(event.screenPoint, map.getInfoWindowAnchor(event.screenPoint));

          var station_node = event.graphic.getNode();
          fx.animateProperty({
              node: station_node,
              duration: 1500,
              properties: {
                  "height": function (station_node) {
                      return {
                          start: 45,
                          end: Number(station_node.getAttribute("height"))
                      };
                  },
                  "width": function (station_node) {
                      return {
                          start: 30,
                          end: Number(station_node.getAttribute("width"))
                      };
                  },
                  "opacity": function (station_node) {
                      return {
                          start: 0.8,
                          end: Number(station_node.getAttribute("opacity"))
                      };
                  }
              }
          }).play();
      });;


      function refreshList() {
          var addressArray = [];
          var XCoordArray = [];
          var YCoordArray = [];
          var FloodZoneArray = [];
          var OutofCityLimitsArray = [];
          var MMTDZoneArray = [];
          var HPOZoneArray = [];
          selectionObjectIDs = [];

          for (var i = 0; i < gl.graphics.length; i++) {
              var attribute = gl.graphics[i].attributes;
              var addressValue = attribute[mapConfig.SiteAddressFlyr.AddressField] || attribute[mapConfig.EsriLocatorAddressField];
              if (!addressValue) continue;
              selectionObjectIDs.push(attribute["OBJECTID"]);
              addressArray.push(addressValue);
              if (attribute["FlodZone"]) FloodZoneArray.push(addressValue);
              if (attribute["OutofCityLimits"]) OutofCityLimitsArray.push(addressValue);
              if (attribute["MMTDZone"]) MMTDZoneArray.push(addressValue);
              if (attribute["HPOZone"]) HPOZoneArray.push(addressValue);
              XCoordArray.push(attribute["POINT_X"]);
              YCoordArray.push(attribute["POINT_Y"]);
          }

          var AddressString = addressArray.join(' | '), XCoordString = XCoordArray.join(' | '), YCoordString = YCoordArray.join(' | ');
          if (FloodZoneArray.length) $("#FloodZoneAlertMsg").text("The following addresses are in Flood Zone: " + FloodZoneArray.join(';'));

          // stop application process if selected address
          // is out side of city limits location with
          // the exception of case types CMGBI_LCGT and GMBI_WATER
          var cityZone = $('#cityZone').val();
          //cityZone = cityZone.substr(0, cityZone.indexOf(':'));
          if (OutofCityLimitsArray.length && cityZone !== 'GMBI_LCGT' && cityZone !== 'GMBI_WATER' ) {
              $("#CityZoneAlertMsg").text("The following addresses are out of City Limits: " + OutofCityLimitsArray.join(';'));
              $('.modal-body').text('Selected Feature is out of City Limits!');
              $('#load-modal').modal('hide');
              $('#myModal').modal();
              $('#next').prop("disabled",true);
          } else $("#CityZoneAlertMsg").text("");
          if (MMTDZoneArray.length) $("#MMTDZoneAlertMsg").text("Addresses in MMTD Zones: " + MMTDZoneArray.join(';')); else $("#MMTDZoneAlertMsg").text("");
        if (HPOZoneArray.length) $("#HPOZoneAlertMsg").text("Addresses in HPO Zones: " + HPOZoneArray.join(';')); else $("#HPOZoneAlertMsg").text("");
          $(".selected-address-two, .selected-address-three").val(AddressString);
          $(".selected-address-two, .selected-address-three").text(AddressString);
          $('.selected-address-cx').val(XCoordString);
          $('.selected-address-cy').val(YCoordString);
          $("#Location").val(AddressString);
          $("#Location").text(AddressString);
          $("#caLocation").val(AddressString);
          $("#caXCoord").val(XCoordString);
          $("#caYCoord").val(YCoordString);
      }

      var btn = dom.byId("zoom2Features");

      on(btn, "click", function () {
          if (gl.graphics.length) {
              var geom = [];
              for (var i = 0; i < gl.graphics.length; i++) {
                  geom.push(gl.graphics[i].geometry);
              }
              var union = geometryEngine.union(geom);
              if (union.type == "point") {
                  map.centerAndZoom(union, 17);
              }
              else {
                  map.setExtent(union.getExtent().expand(2));
              }
          }
      });

      var s = new Search({
          map: map
      }, "location");

      var searchBtnSources = s.get("sources");

      searchBtnSources.push({
          featureLayer: SiteAddressFlyr,
          searchFields: [mapConfig.SiteAddressFlyr.AddressField],
          exactMatch: false,
          outFields: ["*"],
          name: "Search Address",
          placeholder: "ex. 3718 Main St",
          maxResults: 6,
          maxSuggestions: 6,
          infoTemplate: template,
          enableSuggestions: true,
          minCharacters: 0
      });


      searchBtnSources.push({
          featureLayer: SiteAddressFlyr,
          searchFields: mapConfig.SiteAddressFlyr.PINField,
          exactMatch: false,
          name: "Search PIN",
          outFields: ["*"],
          placeholder: "ex. 6835-29-4819.00",
          maxResults: 6,
          maxSuggestions: 6,
          infoTemplate: template,
          enableSuggestions: true,
          minCharacters: 0
      });

      searchBtnSources.push({
          featureLayer: SiteAddressFlyr,
          searchFields: mapConfig.SiteAddressFlyr.BlockNbr,
          exactMatch: false,
          name: "Search Block/Lot",
          outFields: ["*"],
          placeholder: "ex. 0206 500",
          maxResults: 6,
          maxSuggestions: 6,
          infoTemplate: template,
          enableSuggestions: true,
          minCharacters: 0
      });

      //Set the sources above to the search widget
      s.set("sources", searchBtnSources);

      s.startup();

      s.on("select-result", function (event) {
          delete event.source.highlightSymbol;
          populateWithFeatureInfo(event.result.feature);
      });

      function populateWithFeatureInfo(feature) {
          var promises = [];
          var idfyGra = new Graphic(feature.geometry, identifySymbol, feature.attributes, template);

          var propInfoQuery = new Query();
          propInfoQuery.outFields = mapConfig.ParcelPropInfoFLyr.OutFields;
          propInfoQuery.geometry = feature.geometry;
          propInfoQuery.returnGeometry = false;
          promises.push(parcelPropInfoQueryTask.execute(propInfoQuery));

          var MMTDQuery = new Query();
          MMTDQuery.geometry = feature.geometry;
          promises.push(MMTDQueryTask.execute(MMTDQuery));

          var HPOQuery = new Query();
          HPOQuery.geometry = feature.geometry;
          promises.push(HPOQueryTask.execute(HPOQuery));

          all(promises).then(function (results) {
              var propInfoResult = results[0], MMTDResult = results[1], HPOResult = results[2];
              var FloodVal = propInfoResult.features.length > 0 ? propInfoResult.features[0].attributes[mapConfig.ParcelPropInfoFLyr.FloodZoneProperty] : mapConfig.ParcelPropInfoFLyr.NoFloodZoneValues[0];
              var CityVal = propInfoResult.features.length > 0? propInfoResult.features[0].attributes[mapConfig.ParcelPropInfoFLyr.CityZoneProperty] : undefined;
              var MMTDZone = MMTDResult.features.length > 0 ? true : false;
              var HPOZone = HPOResult.features.length > 0 ? true : false;
              if (mapConfig.ParcelPropInfoFLyr.NoFloodZoneValues.indexOf(FloodVal) == -1) idfyGra.attributes["FloodZone"] = true;
              if (mapConfig.ParcelPropInfoFLyr.CityInValues.indexOf(CityVal) == -1) idfyGra.attributes["OutofCityLimits"] = true;
              if (MMTDZone) idfyGra.attributes["MMTDZone"] = true;
              if (HPOZone) idfyGra.attributes["HPOZone"] = true;
              gl.add(idfyGra);
          });


          function queryByIds(ids)
          {
              if (ids.length) {
                  var query = new Query();
                  query.objectIds = ids;
                  query.outFields = ["*"];
                  SiteAddressFlyr.queryFeatures(query, function (event) {
                      var features = event.featureSet;
                      for (var feature in features)
                          populateWithFeatureInfo(feature);
                  }, function (err) {
                      console.log("Problem with populating features!");
                  });
              }              
          }
      }
  });

