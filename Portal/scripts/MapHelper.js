﻿require([

        "esri/map",
        "esri/graphic",
        "esri/dijit/Search",
        "esri/layers/FeatureLayer",
        "esri/layers/ArcGISDynamicMapServiceLayer",
        "esri/layers/GraphicsLayer",
        "esri/InfoTemplate",
        "esri/symbols/SimpleFillSymbol",
        "esri/symbols/SimpleLineSymbol",
        'esri/symbols/PictureMarkerSymbol',
        "esri/tasks/IdentifyTask",
        "esri/tasks/IdentifyParameters",
        "esri/tasks/query",
        "esri/tasks/QueryTask",
        "esri/dijit/Popup",
        "dojo/_base/array",
        "esri/Color",
        "dojo/dom-construct",
        "dojo/promise/all",
        "dojo/domReady!"

],
    function (
        Map, Graphic, Search, FeatureLayer, ArcGISDynamicMapServiceLayer, GraphicsLayer, InfoTemplate, SimpleFillSymbol,
        SimpleLineSymbol, PictureMarkerSymbol, IdentifyTask, IdentifyParameters, Query, QueryTask, Popup, arrayUtils, Color, domConstruct, all
    ) {
        /***********************************
         *            Map Config           *
         ***********************************/
        var mapConfig = {
            extent: {
                "xmin": -9382513.8241863,
                "ymin": 3559906.181829513,
                "xmax": -9381461.620718902,
                "ymax": 3560503.3461129405,
                "spatialReference": {
                    "wkid": 102100
                }
            },
            ParcelMpSVcURL: "http://tlcgisinter.leoncountyfl.gov/arcgis/rest/services/Public/COT_Inter_Cityworks_PLL_D_WM/MapServer",
            SiteAddressFlyr: {
                URL: "http://tlcgisinter.leoncountyfl.gov/arcgis/rest/services/Public/COT_Inter_Cityworks_PLL_D_WM/MapServer/0",
                OutFields: ["*"],
                AddressField: "SITEADDR",
                InfoTemplateFields: {
                    "Address": "SITEADDR",
                    "X Coordinate:": "POINT_X",
                    "Y Coordinate": "POINT_Y"
                }
            },
            EsriLocatorAddressField: "StAddr",
            ParcelPropInfoFLyr: {
                URL: "http://tlcgisinter.leoncountyfl.gov/arcgis/rest/services/Public/TLC_Inter_PropertyInfo_D_WM/MapServer/2",
                OutFields: ["FLDZONE", "CITYINOUT"],
                FloodZoneProperty: "FLDZONE",
                NoFloodZoneValues: ["None"],
                CityZoneProperty: "CITYINOUT",
                CityInValues: ["IN"]
            },
            MMTDFLyrURL: "http://tlcgisinter.leoncountyfl.gov/arcgis/rest/services/Public/TLC_Inter_OverlaySpecialPlanningAreas_WM_D/MapServer/0",
            HPOFLyrURL: "http://tlcgisinter.leoncountyfl.gov/arcgis/rest/services/Public/TLC_Inter_OverlaySpecialPlanningAreas_WM_D/MapServer//1"
        };

        var ext = new esri.geometry.Extent(
            mapConfig.extent
        );

        var map = new Map("map", {
            basemap: "topo",
            extent: ext,
            infoWindow: popup
        });

        // add address feature layer to map
        var siteAddressFlyr = new FeatureLayer(mapConfig.SiteAddressFlyr.URL, {
            mode: FeatureLayer.MODE_ONDEMAND,
            outFields: mapConfig.SiteAddressFlyr.OutFields
            //infoTemplate: template
        });
        map.addLayer(siteAddressFlyr);
        map.addLayer(new ArcGISDynamicMapServiceLayer(mapConfig.ParcelMpSVcURL, {
            opacity: 0.55
        }));
        var gl = new GraphicsLayer();
        map.addLayer(gl);

        gl.on("graphic-add", function (graphic) {
            refreshList();
        });
        var Statusflag = false;
        siteAddressFlyr.on("click", function (event) {
            var cityZone = $('#cityZone').val();
            (cityZone === 'GMBI_AS') ? (cityZone = $('#cityZoneSubType').val()) : cityZone;
            if (!getSingleAddressPermitType(cityZone) || !(Statusflag)) {
                populateFeatureInfo(event.graphic);
            } else {
                $('.modal-body').text('Only one address can be selected for this permit type');
                $('#load-modal').modal('hide');
                $('#myModal').modal();
            }
        });

        //Event to remove the selected graphic(address) from the selection
        gl.on("click", function (event) {
            event.preventDefault();
            gl.remove(event.graphic);
            localStorage.removeItem("_address");
            refreshList();
            Statusflag = false;
        });
        //Clear Map points.
        $('#clear').on("click", function (event) {
            $("#FloodZoneAlertMsg").text("");
            $("#CityZoneAlertMsg").text("");
            $("#MMTDZoneAlertMsg").text("");
            $("#HPOZoneAlertMsg").text("");
            $("#Location").text("");
            gl.clear();
            Statusflag = false;
             
        });

        var identifySymbol = new PictureMarkerSymbol({
            "angle": 0,
            "xoffset": 0,
            "yoffset": 5,
            "type": "esriPMS",
            "url": "http://img3.wikia.nocookie.net/__cb20140427224234/caramelangel714/images/7/72/Location_Icon.png",
            "contentType": "image/png",
            "width": 18,
            "height": 27
        });

        /***********************************
         *            Query Task           *
         ***********************************/
        var parcelPropInfoQueryTask = new QueryTask(mapConfig.ParcelPropInfoFLyr.URL);
        var MMTDQueryTask = new QueryTask(mapConfig.MMTDFLyrURL);
        var HPOQueryTask = new QueryTask(mapConfig.HPOFLyrURL);

        /***********************************
         *            Search                *
         ***********************************/
        var search = new Search({
            enableInfoWindow: true,
            showInfoWindowOnSelect: true,
            sources: [],
            map: map
        }, "search");
        // search sources.  limited to address and pin per COT.
        var sources = search.get("sources");
        // push sources for search. disable default ArcGIS geocoder per COT.
        // address source
        sources.push({
            featureLayer: new FeatureLayer("http://tlcgisinter.leoncountyfl.gov/arcgis/rest/services/Public/COT_Inter_Cityworks_PLL_D_WM/MapServer/0"),
            searchFields: ["SITEADDR"],
            exactMatch: false,
            outFields: ["*"],
            name: "Search Address",
            placeholder: "Type in address",
            maxResults: 6,
            maxSuggestions: 6,
            //enableSuggestions: true,
            suggestionTemplate: "${SITEADDR}",
            minCharacters: 0
        });
        // set sources for search widget
        search.set("sources", sources);
        search.startup();
        search.on('select-result', function (event) {
            var cityZone = $('#cityZone').val();
            (cityZone === 'GMBI_AS') ? (cityZone = $('#cityZoneSubType').val()) : cityZone;
            if (!getSingleAddressPermitType(cityZone) || !(Statusflag)) {
                populateFeatureInfo(event.result.feature);
                // identifyTask is used to pull both the address and parcel info for a searched address based on the feature's geometry
                //execIdentifyTask(event.result.feature.geometry);
            } else {
                $('.modal-body').text('Only one address can be selected for this permit type');
                $('#load-modal').modal('hide');
                $('#myModal').modal();
            }
        });

        function populateFeatureInfo(feature) {
            var promises = [];
            var idfyGra = new Graphic(feature.geometry, identifySymbol, feature.attributes);
            var propInfoQuery = new Query();
            propInfoQuery.outFields = mapConfig.ParcelPropInfoFLyr.OutFields;
            propInfoQuery.geometry = feature.geometry;
            propInfoQuery.returnGeometry = false;
            promises.push(parcelPropInfoQueryTask.execute(propInfoQuery));

            var mmtdQuery = new Query();
            mmtdQuery.geometry = feature.geometry;
            promises.push(MMTDQueryTask.execute(mmtdQuery));

            var hpoQuery = new Query();
            hpoQuery.geometry = feature.geometry;
            promises.push(HPOQueryTask.execute(hpoQuery));
            all(promises).then(function (results) {
                var propInfoResult = results[0],
                    mmtdResult = results[1],
                    hpoResult = results[2];
                var floodVal = propInfoResult.features.length > 0 ? propInfoResult.features[0].attributes[mapConfig.ParcelPropInfoFLyr.FloodZoneProperty] : mapConfig.ParcelPropInfoFLyr.NoFloodZoneValues[0];
                var cityVal = propInfoResult.features.length > 0 ? propInfoResult.features[0].attributes[mapConfig.ParcelPropInfoFLyr.CityZoneProperty] : undefined;
                var mmtdZone = mmtdResult.features.length > 0 ? true : false;
                var hpoZone = hpoResult.features.length > 0 ? true : false;
                if (mapConfig.ParcelPropInfoFLyr.NoFloodZoneValues.indexOf(floodVal) === -1) idfyGra.attributes["FloodZone"] = true;
                if (mapConfig.ParcelPropInfoFLyr.CityInValues.indexOf(cityVal) === -1) idfyGra.attributes["OutofCityLimits"] = true;
                if (mmtdZone) idfyGra.attributes["MMTDZone"] = true;
                if (hpoZone) idfyGra.attributes["HPOZone"] = true;
                gl.add(idfyGra);
            });
        }

        /***********************************
         *            IdentifyTask          *
         ***********************************/
        // experimental IdentifyTask
        var identifyTask, identifyParams;
        var popup = new Popup({
                fillSymbol: new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
                    new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
                        new Color([255, 0, 0]),
                        2),
                    new Color([255, 255, 0, 0.25]))
            },
            domConstruct.create("div"));

        //Queries the address and parcel layers and displays the infowindow to show the layer attributes.
        function execIdentifyTask(geometry) {
            identifyTask = new IdentifyTask(mapConfig.ParcelMpSVcURL);
            identifyParams = new IdentifyParameters();
            identifyParams.tolerance = 3;
            identifyParams.returnGeometry = true;
            identifyParams.layerIds = [0, 1];
            identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_ALL;
            identifyParams.width = map.width;
            identifyParams.height = map.height;
            identifyParams.geometry = geometry;
            identifyParams.mapExtent = map.extent;
            var addr = null;
            var deferred = identifyTask
                .execute(identifyParams)
                .addCallback(function (response) {
                    //Response is an array of identify result objects.
                    return arrayUtils.map(response, function (result) {
                        var feature = result.feature;
                        var layerName = result.layerName;
                        var attribute = feature.attributes;
                        feature.attributes.layerName = layerName;
                        if (layerName === 'Site Addresses') {
                            var addressTemplate = new InfoTemplate("Address Info", "Address: ${SITEADDR}</br>Object ID: ${OBJECTID}</br>X: ${POINT_X}</br>Y: ${POINT_Y}");
                            feature.setInfoTemplate(addressTemplate);
                            addr = new Address(attribute["SITEADDR"], attribute["OBJECTID"], attribute["CW_ASSET"], attribute["HOUSENUM"], attribute["NAME"], attribute["PREFIX"], attribute["ST_ADDRHLF"], attribute["TYPE"], null, null, attribute["X"], attribute["Y"]);
                            addressList.push(addr);
                        } else if (layerName === 'Property Parcels') {
                            var parcelTemplate = new InfoTemplate("Parcel Info", "Address: ${SITEADDR}</br>Object ID: ${OBJECTID}</br>Tax ID: ${TAXID}</br>Tax District: ${TAXDIST}");
                            feature.setInfoTemplate(parcelTemplate);
                            addr = new Address(attribute["SITEADDR"], attribute["OBJECTID"], attribute["TAXID"], attribute["HOUSENUM"], attribute["NAME"], attribute["PREFIX"], attribute["ST_ADDRHLF"], attribute["TYPE"], attribute["ST_APT_STE"], attribute["ZIP1"], attribute["X"], attribute["Y"]);
                            addressList.push(addr);
                        }
                        var jsonAddr = JSON.stringify(addressList);
                        localStorage.setItem('_address', jsonAddr);
                        return feature;
                    });
                });

            map.infoWindow.setFeatures([deferred]);
            map.infoWindow.show(geometry);
        }
        var addressList = [];

        function Address(siteAddr, addressId, assetId, houseNum, name, prefix, fraction, type, suite, zip, x, y) {
            this.AddressId = addressId;
            this.AssetId = assetId;
            this.AssetType = null;
            this.CityName = null;
            this.FeatureAssetId = null;
            this.FeatureClass = null;
            this.FeatureObjectId = addressId;
            this.Location = siteAddr;
            this.StateCode = null;
            this.StreetDirection = prefix;
            this.StreetFraction = fraction;
            this.StreetName = name;
            this.StreetNumber = houseNum;
            this.StreetType = type;
            this.Suite = suite;
            this.XCoord = x;
            this.YCoord = y;
            this.ZipCode = zip;
        }

        function refreshList() {
            var addressArray = [];
            var xCoordArray = [];
            var YCoordArray = [];
            var FloodZoneArray = [];
            var OutofCityLimitsArray = [];
            var MMTDZoneArray = [];
            var HPOZoneArray = [];
            var IsOutofCityLimits;
            selectionObjectIDs = [];
            addressList = [];
            //var addressList = [];
            var cityZone = $('#cityZone').val();
            for (var i = 0; i < gl.graphics.length; i++) {
                var attribute = gl.graphics[i].attributes;
                var addressValue = attribute[mapConfig.SiteAddressFlyr.AddressField] || attribute[mapConfig.EsriLocatorAddressField];
                if (!addressValue) continue;
                execIdentifyTask(gl.graphics[i].geometry);
                selectionObjectIDs.push(attribute["OBJECTID"]);
                addressArray.push(addressValue);
                Statusflag = true;
                if (attribute["FloodZone"]) FloodZoneArray.push(addressValue);
                if (attribute["OutofCityLimits"]) {
                    OutofCityLimitsArray.push(addressValue);
                    IsOutofCityLimits = true;
                } else {
                    IsOutofCityLimits = false;
                }
                if (attribute["MMTDZone"]) MMTDZoneArray.push(addressValue);
                if (attribute["HPOZone"]) HPOZoneArray.push(addressValue);
                xCoordArray.push(attribute["POINT_X"]);
                YCoordArray.push(attribute["POINT_Y"]);
            }
            var AddressString = addressArray.join(' | '),
                XCoordString = xCoordArray.join(' | '),
                YCoordString = YCoordArray.join(' | ');
            if (FloodZoneArray.length) {
                $("#FloodZoneAlertMsg").text("The following addresses are in Flood Zone: " + FloodZoneArray.join(';'));
            }

            function refreshList() {
                var addressArray = [];
                var XCoordArray = [];
                var YCoordArray = [];
                var FloodZoneArray = [];
                var OutofCityLimitsArray = [];
                var MMTDZoneArray = [];
                var HPOZoneArray = [];
                var IsOutofCityLimits;             
                selectionObjectIDs = [];
                //
                var cityZone = $('#cityZone').val();
                for (var i = 0; i < gl.graphics.length; i++) {
                    var attribute = gl.graphics[i].attributes;
                    var addressValue = attribute[mapConfig.SiteAddressFlyr.AddressField] || attribute[mapConfig.EsriLocatorAddressField];
                    if (!addressValue) continue;
                    selectionObjectIDs.push(attribute["OBJECTID"]);    
                    addressArray.push(addressValue); Statusflag = true;
                    if (attribute["FloodZone"]) FloodZoneArray.push(addressValue);
                    if (attribute["OutofCityLimits"]) {
                        OutofCityLimitsArray.push(addressValue);
                        IsOutofCityLimits=true;
                    } else
                    {
                        IsOutofCityLimits=false;
                    }
                    if (attribute["MMTDZone"]) MMTDZoneArray.push(addressValue);
                    if (attribute["HPOZone"]) HPOZoneArray.push(addressValue);
                    XCoordArray.push(attribute["POINT_X"]);
                    YCoordArray.push(attribute["POINT_Y"]);
                }  
                var AddressString = addressArray.join(' | '),
                    XCoordString = XCoordArray.join(' | '),
                    YCoordString = YCoordArray.join(' | ');
                if (FloodZoneArray.length) {
                    $("#FloodZoneAlertMsg").text("The following addresses are in Flood Zone: " + FloodZoneArray.join(';'));
                }
                else $("#FloodZoneAlertMsg").text("");

                // stop application process if selected address
                // is out side of city limits location with
                // the exception of case types CMGBI_LCGT and GMBI_WATER  
                if (IsOutofCityLimits && cityZone !== 'GMBI_LCGT' && cityZone !== 'GMBI_WATER') {
                    $("#CityZoneAlertMsg").text("The following addresses are out of City Limits: " + OutofCityLimitsArray.join(';'));
                    $('.modal-body').text('Selected Feature is out of City Limits!');
                    $('#load-modal').modal('hide');
                    $('#myModal').modal();
                    $('#next').prop("disabled", true);
                } else {
                    if (!OutofCityLimitsArray.length) {
                        $("#CityZoneAlertMsg").text("");
                        $('#next').prop("disabled", false);
                    }
                }
                if (MMTDZoneArray.length) $("#MMTDZoneAlertMsg").text("Addresses in MMTD Zones: " + MMTDZoneArray.join(';'));
                else $("#MMTDZoneAlertMsg").text("");
                if (HPOZoneArray.length) {
                    sessionStorage.setItem("HPO", HPOZoneArray.join(';'));
                    $("#HPOZoneAlertMsg").text("Addresses in HPO Zones: " + HPOZoneArray.join(';'));
                }
                else {
                    sessionStorage.removeItem("HPO");
                    $("#HPOZoneAlertMsg").text("");
                }
                $(".selected-address-two, .selected-address-three").val(AddressString);
                $(".selected-address-two, .selected-address-three").text(AddressString);
                $('.selected-address-cx').val(XCoordString);
                $('.selected-address-cy').val(YCoordString);
                $("#Location").val(AddressString);
                $("#Location").text(AddressString);
                $("#caLocation").val(AddressString);
                $("#caXCoord").val(XCoordString);
                $("#caYCoord").val(YCoordString);
            }
        
        if (MMTDZoneArray.length) $("#MMTDZoneAlertMsg").text("Addresses in MMTD Zones: " + MMTDZoneArray.join(';'));
        else $("#MMTDZoneAlertMsg").text("");
        if (HPOZoneArray.length) $("#HPOZoneAlertMsg").text("Addresses in HPO Zones: " + HPOZoneArray.join(';'));
        else $("#HPOZoneAlertMsg").text("");
        $(".selected-address-two, .selected-address-three").val(AddressString);
        $(".selected-address-two, .selected-address-three").text(AddressString);



        $('.selected-address-cx').val(XCoordString);
        $('.selected-address-cy').val(YCoordString);
        $("#Location").val(AddressString);
        $("#Location").text(AddressString);
        $("#caLocation").val(AddressString);
        $("#caXCoord").val(XCoordString);
        $("#caYCoord").val(YCoordString);
    }

        function getSingleAddressPermitType($PERMIT_TYPE) {
            return $PERMIT_TYPE === 'GMBI_CBLD' || $PERMIT_TYPE === 'GMBI_ELEC' || $PERMIT_TYPE === 'GMBI_MECH' || $PERMIT_TYPE === 'GMBI_GAS' || $PERMIT_TYPE === 'GMBI_PLUM' || $PERMIT_TYPE === 'GMBI_ROOF' || $PERMIT_TYPE === 'GMBI_ASP' || $PERMIT_TYPE === 'GMBI_ASG' || $PERMIT_TYPE === 'GMLU_ZONE' || $PERMIT_TYPE === 'GMBI_LCGT';
        }
    });