﻿using Portal.Models.DB;
using Portal.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Portal
{
    public static class DateTimeExtensions
    {
        //public static NextDays GetWorkDays()
        //{
        //    var nextDays = new NextDays();
        //    var testDate = DateTime.Now.Date;

        //    while (nextDays.Dates.Count() < 5)
        //    {
        //        if (testDate.DayOfWeek != DayOfWeek.Saturday &&
        //                 testDate.DayOfWeek != DayOfWeek.Sunday &&
        //                 !testDate.IsHoliday())
        //        {
        //            nextDays.Dates.Add(testDate);
        //            nextDays.DaysOfWeek.Add(testDate.ToString("dddd " + testDate.ToString("MM/dd/yy") ));
        //        }
        //        testDate = testDate.AddDays(1);
        //    }

        //    return nextDays;
        //}

        public static bool IsHoliday(this DateTime date)
        {
            CityworksEntities db = new CityworksEntities();
            var tab = (from h in db.HOLIDAYS_VW
                       select h.HOLIDAY_DATE);
            List<DateTime> holidays = new List<DateTime>();
            foreach (var t in tab)
            {
                holidays.Add(t);
            }

            return holidays.Contains(date.Date);
        }
    }
}