﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Portal.Models.DB;
using Portal.Models.ViewModel;
using System.Web.Services;
using System.IO;

namespace Portal.Controllers
{
    public class PermitInfoController : Controller
    {
        private CityworksEntities db = new CityworksEntities();
       
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult GetPermitInfo(string permitNum)
        {
            var tab = (from c in db.CA_OBJECT_VW
                      where c.CASE_NUMBER.Contains(permitNum)
                      select c).ToList();
            return PartialView("_PermitInfo", tab);
        }

        public ActionResult GetPermitLocation(string permitNum)
        {
            var tab = (from a in db.CA_ADDRESS_VW
                       where a.CASE_NUMBER.Contains(permitNum)
                       select a).ToList();
            return PartialView("_PermitLocation", tab);
        }

        public ActionResult GetPermitPeople(string permitNum)
        {
            var tab = (from p in db.CA_PEOPLE_VW
                       where p.CASE_NUMBER.Contains(permitNum)
                       select p).ToList();
            return PartialView("_PermitPeople", tab);
        }

        public ActionResult GetPermitTasks(string permitNum)
        {
            var tab = (from t in db.CA_TASK_VW
                       where t.CASE_NUMBER.Contains(permitNum)
                       orderby t.TASK_ID
                       select t).ToList();
            return PartialView("_PermitTasks", tab);
        }

        public ActionResult GetPermitInspections(string permitNum)
        {
            var tab = (from i in db.CA_INSPECTION_REQ_VW
                       where i.CASE_NUMBER.Contains(permitNum)
                       select i).ToList();
            return PartialView("_PermitInspections", tab);
        }

        public ActionResult GetPermitFees(string permitNum)
        {
            var tab = (from f in db.CA_FEES_VW
                       where f.CASE_NUMBER.Contains(permitNum)
                       select f).ToList();
            return PartialView("_PermitFees", tab);
        }

        public ActionResult GetPermitPayments(string permitNum)
        {
            var tab = (from p in db.CA_PAYMENT_VW
                       where p.CASE_NUMBER.Contains(permitNum)
                       select p).ToList();
            return PartialView("_PermitPayments", tab);
        }

        
    }
}