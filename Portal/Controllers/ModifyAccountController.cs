﻿using Portal.Models.DB;
using Portal.Models.EntityManager;
using Portal.Models.ViewModel;
using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;

namespace Portal.Controllers
{
    public class ModifyAccountController : Controller
    {
        private CityworksEntities db = new CityworksEntities();

        
        public ActionResult Index()
        {
            UserViewModel model = new UserViewModel();
            var claimsIdentity = User.Identity as ClaimsIdentity;
            int userId = Convert.ToInt32(claimsIdentity.FindFirst(ClaimTypes.NameIdentifier).Value);
            USER user = (from u in db.USERS
                         where u.USER_ID == userId
                         select u).FirstOrDefault();
            model.User = user;
            CONTRACTOR_VW contractor = (from c in db.CONTRACTOR_VW
                                        where c.USER_ID == userId
                                        select c).FirstOrDefault();
            model.Contractor = contractor;
            return View(model);
        }

        [HttpPost]
        public ActionResult SaveModifications(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                USER user = (from u in db.USERS
                             where u.LOGIN_ID == model.User.LOGIN_ID
                             select u).FirstOrDefault();
                UserManager UM = new UserManager();
               

                if (model.ModifyAccount.NewPassword != null)
                {
                    string currentPassword = UM.GetMD5Hash(model.ModifyAccount.Password);
                    if (user.PASSWORD != currentPassword)
                    {
                        ModelState.AddModelError("ModifyAccount.Password", "Current password is incorrect.");
                        return View("Index", model);
                    }

                    string newPassword = UM.GetMD5Hash(model.ModifyAccount.NewPassword);
                    user.PASSWORD = newPassword;
                }

                user.EMAIL_ID = model.User.EMAIL_ID;
                user.FIRST_NAME = model.User.FIRST_NAME;
                user.LAST_NAME = model.User.LAST_NAME;
                user.ADDRESS_LINE1 = model.User.ADDRESS_LINE1;
                user.ADDRESS_LINE2 = model.User.ADDRESS_LINE2;
                user.CITY_NAME = model.User.CITY_NAME;
                user.STATE_CODE = model.User.STATE_CODE;
                user.ZIP_CODE = model.User.ZIP_CODE;
                user.COUNTRY_CODE = model.User.COUNTRY_CODE;
                user.PHONE_HOME = model.User.PHONE_HOME;
                user.PHONE_MOBILE = model.User.PHONE_MOBILE;
                
                //CONTRACTOR contractor = (from c in db.CONTRACTORs
                //                         where c.BUSINESS_NAME == model.Contractor.BUSINESS_NAME
                //                         select c).FirstOrDefault();
                //contractor.BUSINESS_NAME = model.Contractor.BUSINESS_NAME;
                //contractor.ADDRESS_LINE1 = model.Contractor.ADDRESS_LINE1;
                //contractor.CITY_NAME = model.Contractor.CITY_NAME;
                //contractor.STATE_CODE = model.Contractor.STATE_CODE;
                //contractor.ZIP_CODE = model.Contractor.ZIP_CODE;
                //contractor.PHONE_WORK = model.Contractor.PHONE_WORK;
                //contractor.PHONE_MOBILE = model.Contractor.PHONE_MOBILE;

                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var entityValidationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in entityValidationErrors.ValidationErrors)
                        {
                            Response.Write("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                        }
                    }
                }
                return View("Index", model);
            }
            return View(model);
        }
    }
}