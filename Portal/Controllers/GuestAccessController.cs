﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Claims;

namespace Portal.Controllers
{
    public class GuestAccessController : Controller
    {
        [Authorize]
        // GET: GuestAccess
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult LogIn()
        {
            string EncodedResponse = Request.Form["g-recaptcha-response"];
            string isCaptchaValid = Recaptcha.Validate(EncodedResponse);
            if (isCaptchaValid == "True")
            {

                var identity = new ClaimsIdentity(new[] {
                            new Claim(ClaimTypes.Name, "Guest"),
                            new Claim(ClaimTypes.Role, "GUEST")
                        },
                        "ApplicationCookie");

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;

                authManager.SignIn(identity);

                return RedirectToAction("Welcome", "Home");
            }
            return View("Index");
        }
    }
}