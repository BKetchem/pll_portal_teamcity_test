﻿using Cityworks.Core;
using PayPal.Api;
using Portal.Models.DB;
using Portal.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using log4net;

namespace Portal.Controllers
{
    public class PaymentsController : Controller
    {
        private CityworksEntities db = new CityworksEntities();
        private Uri serviceUri = new Uri(ServerUrl);
        public static string ServerUrl = ConfigurationManager.AppSettings["servicesUrl"];
        private const string SESSION_SERVICE_FACTORY = "ServiceFactory";
        private ErrorModel error = new ErrorModel();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
                (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private Services factory
        {
            get { return (Services)Session[SESSION_SERVICE_FACTORY]; }
            set { Session[SESSION_SERVICE_FACTORY] = value; }
        }

        [HttpGet]
        public ActionResult SearchNumber()
        {
            return View();
        }

       

        [HttpPost]
        public ActionResult SearchNumber(string caseNumber)
        {
            var fees = from c in db.CA_FEES_VW
                       where c.CASE_NUMBER.Contains(caseNumber) & c.AMOUNT > c.PAYMENT_AMOUNT & c.WAIVE_FEE == "false" & Math.Round(c.AMOUNT, 2) > Math.Round(c.PAYMENT_AMOUNT, 2)
                       select c;
            ViewBag.Message = caseNumber;
            return View("SearchNumberResults", fees);
        }

        [HttpGet]
        public ActionResult SearchLocation()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SearchLocation(string address)
        {
            var fees = from t1 in db.CA_FEES_VW
                       join t2 in db.CA_OBJECT_VW on t1.CA_OBJECT_ID equals t2.CA_OBJECT_ID
                       where t2.LOCATION.ToUpper().Contains(address) & t1.AMOUNT > t1.PAYMENT_AMOUNT & t1.WAIVE_FEE == "false" & Math.Round(t1.AMOUNT, 2) > Math.Round(t1.PAYMENT_AMOUNT, 2)
                       select t1;
            return View("SearchLocationResults", fees); ;
        }

        [HttpPost]
        public ActionResult MakePayment(string caseNumber, string paymentMethod)
        {

            log.Info("Begin processing in MakePayment Controller.");

            string user = ConfigurationManager.AppSettings["user"];
            string pw = ConfigurationManager.AppSettings["pw"];
            factory = new Services(serviceUri);
            string requestor = User.Identity.Name;
            var caObjectId = Convert.ToInt64((from t in db.CA_OBJECT
                                              where t.CASE_NUMBER.ToUpper().Equals(caseNumber.ToUpper())
                                              select t.CA_OBJECT_ID).FirstOrDefault());
            var tenderTypeId = Convert.ToInt64((from t in db.TENDER_TYPE
                                                where t.TENDER_TYPE1.ToUpper().Contains(paymentMethod.ToUpper())
                                                select t.TENDER_TYPE_ID).FirstOrDefault());
            decimal totalAmount = 0;
            List<CA_PAYMENT_VW> model = new List<CA_PAYMENT_VW>();
            var authResponse = factory.Authentication(new AuthenticationService.Authenticate { LoginName = user, Password = pw });
            if (authResponse.Status == CoreResponseStatus.Ok)
            {
                log.Info("Cityworks Authentication success.");
                try
                {
                    var fees = (from f in db.CA_FEES_VW
                                where f.AMOUNT > f.PAYMENT_AMOUNT & f.CASE_NUMBER == caseNumber & f.WAIVE_FEE == "false" & Math.Round(f.AMOUNT, 2) > Math.Round(f.PAYMENT_AMOUNT, 2)
                                select f).ToList();

                    for (var i = 0; i < fees.Count(); i++)
                    {
                        if (fees[i].PAYMENT_AMOUNT > 0)
                        {
                            totalAmount = Convert.ToDecimal(totalAmount + (fees[i].AMOUNT - fees[i].PAYMENT_AMOUNT));
                        }
                        else
                        {
                            totalAmount = Convert.ToDecimal(totalAmount + fees[i].AMOUNT);
                        }
                    }

                    totalAmount = (Math.Round(totalAmount, 2));

                    // PayPal transactions are limited to $10,000
                    if (totalAmount > 10000)
                    {
                        error.ErrorMsg = "PayPal transactions are limited to $10,000.";
                        return View("../Error/Index", error);
                    }

                    Session.Add("caObjectId", caObjectId);
                    Session.Add("tenderTypeId", tenderTypeId);
                    Session.Add("caseNumber", caseNumber);
                    Session.Add("fees", fees);
                    Session.Add("totalAmount", totalAmount);
                    if (paymentMethod == "paypal")
                    {
                        log.Info("Begin PayPal method.");
                        return (PaymentWithPaypal(totalAmount.ToString()));
                    }
                    else if(paymentMethod == "credit")                              //Added by Srinivas for Credit card integration.
                    {
                        if (caseNumber!=null)
                        {
                            // string urltext = @"https://securetest10.ipayment.com/tallahassee_test/cbc/start.htm?auth_token=65g32x2&user_id=Web_user&department_id=002&transaction_id=GrowthMngmntWeb_CTT&transaction_data.CaseNumber=" + caseNumber + "&transaction_data.CaObjectId=" + caObjectId + "&transaction_data.auto_continue=true&finished_buying=true";
                            // return new RedirectResult(urltext);
                            return View("PaymentSummary", model);
                        }
                        else
                        {
                            error.ErrorMsg = "CaseNumber not found for this location";
                            return View("../Error/Index", error);
                        }
                                               
                    }
                }
                catch (Exception ex)
                {
                    error.ErrorMsg = ex.Message;
                    return View("../Error/Index", error);
                }
            }
            return View("PaymentSummary", model);
        }

        public ActionResult CityworksPayments(List<CA_FEES_VW> fee, long caObjectId, long tenderTypeId, string caseNumber, string paymentId)
        {
            string user = ConfigurationManager.AppSettings["user"];
            string pw = ConfigurationManager.AppSettings["pw"];
            factory = new Services(serviceUri);
            List<CA_PAYMENT_VW> model = new List<CA_PAYMENT_VW>();
            string requestor = User.Identity.Name;
            CA_PAYMENT_VW payment = null;
            var authResponse = factory.Authentication(new AuthenticationService.Authenticate { LoginName = user, Password = pw });
            if (authResponse.Status == CoreResponseStatus.Ok)
            {
                //for(var i=0; i < fee.Count(); i++)
                foreach (var i in fee)
                {
                    var paymentResponse = factory.PLL.CasePayment(new CasePaymentService.Add
                    {
                        CaFeeId = Convert.ToInt64(i.CA_FEE_ID),
                        CaObjectId = caObjectId,
                        CustFeeSeq = Convert.ToInt64(i.CUST_FEE_SEQ),
                        DateReceived = DateTime.Now,
                        PaymentAmount = i.AMOUNT,
                        PaymentDate = DateTime.Now,
                        ReceivedBy = 1,
                        TenderTypeId = tenderTypeId,
                        ReferenceInfo = paymentId
                    });
                    // need to get fee code and fee desc from here.
                    if (paymentResponse.Status == CoreResponseStatus.Ok)
                    {
                        payment = new CA_PAYMENT_VW();

                        var payments = (from p in db.CA_PAYMENT_VW
                                        where p.CA_PAYMENT_ID == paymentResponse.Value.CaPaymentId
                                        select p).ToList();

                        payment.CASE_NUMBER = caseNumber;
                        payment.CA_FEE_ID = Convert.ToInt64(paymentResponse.Value.CaFeeId);
                        payment.CA_PAYMENT_ID = Convert.ToInt64(paymentResponse.Value.CaPaymentId);
                        payment.PAYMENT_AMOUNT = Convert.ToDecimal(paymentResponse.Value.PaymentAmount);
                        payment.FEE_CODE = payments[0].FEE_CODE;
                        payment.FEE_DESC = payments[0].FEE_DESC;
                        payment.REFERENCE_INFO = paymentResponse.Value.ReferenceInfo;
                        model.Add(payment);
                    }
                }
            }
            return View("PaymentSummary", model);
        }

        public ActionResult PaymentWithPaypal(string totalAmount)
        {
            log.Info("Starting PaymentWithPaypal Controller.");
            //getting the apiContext as earlier
            APIContext apiContext = Portal.Models.EntityManager.Configuration.GetAPIContext();
            try
            {
                string payerId = Request.Params["PayerID"];

                if (string.IsNullOrEmpty(payerId))
                {
                    string baseURI = Request.Url.Scheme + "://" + Request.Url.Authority + "/Portal/Payments/PaymentWithPayPal?";
                    var guid = Convert.ToString((new Random()).Next(100000));
                    var createdPayment = this.CreatePayment(apiContext, baseURI + "guid=" + guid, totalAmount);
                    var links = createdPayment.links.GetEnumerator();
                    string paypalRedirectUrl = null;
                    while (links.MoveNext())
                    {
                        Links lnk = links.Current;
                        if (lnk != null && lnk.rel.ToLower().Trim().Equals("approval_url"))
                        {
                            paypalRedirectUrl = lnk.href;
                        }
                    }
                    Session.Add(guid, createdPayment.id);
                    return Redirect(paypalRedirectUrl);
                }
                else
                {
                    var guid = Request.Params["guid"];
                    var paymentId = Request.Params["paymentId"];
                    var caObjectId = Convert.ToInt64(Session["caObjectId"]);
                    var caseNumber = Session["caseNumber"].ToString();
                    var tenderTypeId = Convert.ToInt64(Session["tenderTypeId"]);
                    // var paymentAmount = Convert.ToDecimal(Session["totalAmount"]);
                    //var fees = Session["fees"] as List<CaFeesItem>;

                    var fees = Session["fees"] as List<CA_FEES_VW>;
                    //CityworksPayments(fees, caObjectId, tenderTypeId, caseNumber, paymentId);
                    var executedPayment = ExecutePayment(apiContext, payerId, Session[guid] as string);
                    if (executedPayment.state.ToLower() != "approved")
                    {
                        error.ErrorMsg = "PayPal payment declined.";
                        return View("../Error/Index", error);
                    }
                    return (CityworksPayments(fees, caObjectId, tenderTypeId, caseNumber, paymentId));
                }
            }
            catch (Exception ex)
            {
                error.ErrorMsg = ex.Message;
                return View("../Error/Index", error);
            }
        }

        private PayPal.Api.Payment payment;

        private Payment ExecutePayment(APIContext apiContext, string payerId, string paymentId)
        {
            try
            {
                var paymentExecution = new PaymentExecution() { payer_id = payerId };
                this.payment = new Payment() { id = paymentId };
                return this.payment.Execute(apiContext, paymentExecution);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private Payment CreatePayment(APIContext apiContext, string redirectUrl, string totalAmount)
        {
            //similar to credit card create itemlist and add item objects to it
            var itemList = new ItemList() { items = new List<Item>() };

            itemList.items.Add(new Item()
            {
                name = "Case Payment",
                currency = "USD",
                price = totalAmount,
                quantity = "1",
                sku = "sku"
            });

            var payer = new Payer() { payment_method = "paypal" };

            // Configure Redirect Urls here with RedirectUrls object
            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl,
                return_url = redirectUrl
            };

            // similar as we did for credit card, do here and create details object
            var details = new Details()
            {
                tax = "0",
                shipping = "0",
                subtotal = totalAmount
            };

            // similar as we did for credit card, do here and create amount object
            var amount = new Amount()
            {
                currency = "USD",
                total = totalAmount, // Total must be equal to sum of shipping, tax and subtotal.
                details = details
            };

            var transactionList = new List<Transaction>();

            transactionList.Add(new Transaction()
            {
                description = "Transaction description.",
                invoice_number = "your invoice number",
                amount = amount,
                item_list = itemList
            });

            this.payment = new Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };

            // Create a payment using a APIContext
            return this.payment.Create(apiContext);
        }
    }
}