﻿using Cityworks.Core;
using Portal.Models.DB;
using Portal.Models.EntityManager;
using Portal.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace Portal.Controllers
{
    public class CreateApplicationController : Controller
    {
        private const string SessionServiceFactory = "ServiceFactory";
        public static string ServerUrl = ConfigurationManager.AppSettings["servicesUrl"];
        public static string caseName;
        private readonly CityworksEntities _db = new CityworksEntities();
        private readonly Uri _serviceUri = new Uri(ServerUrl);
        public object CaDataGroupId { get; private set; }

        private Services Factory
        {
            get { return (Services)Session[SessionServiceFactory]; }
            set { Session[SessionServiceFactory] = value; }
        }

        [HttpPost]
        public ActionResult AddContractor(List<CustomContractor> contractor)
        {
            Session["data"] = null;
            var sessionModel = (CreateApplicationViewModel) Session["case"];
            sessionModel.CaseContractor.ContractorList.Clear();
            List<string> clist = new List<string>();
            if(contractor!=null)
            { 
                foreach (var i in contractor)
                {
                    var contractorlist = new ContractorList
                    {
                        ContractorId = i.ContractorId,
                        ContractorTypeId = i.ContractorTypeId,
                        StateLicenseId = i.StateLicenseId,
                    };
                    clist.Add("Contractor Name: "+i.ContractorName);
                    clist.Add("Contractor Type: "+i.ContractorType);
                    clist.Add("License Num: "+i.License_Num);
                    sessionModel.CaseContractor.ContractorList.Add(contractorlist);
                }                
                Session["data"] = clist;
            }
            sessionModel = GetCaseDataDetails(sessionModel);
            Session["case"] = sessionModel;
            return Json(new { result = "Redirect", url = Url.Action("CaseDataDetails", "CreateApplication") });
        }
/*
        [HttpPost]
        public ActionResult AddContractor(decimal? contractorId, decimal? contractorTypeId, decimal? stateLicenseId)
        {
            Session["data"] = null;
            var sessionModel = (CreateApplicationViewModel)Session["case"];
            if (contractorId != null)
                try
                {
                    sessionModel.Contractor.Clear();
                    var data = (from c in _db.CONTRACTORs
                                join s in _db.STATE_LICENSE_VW
                                on c.CONTRACTOR_ID equals s.CONTRACTOR_ID into state
                                from y in state.DefaultIfEmpty()
                                where
                                (c.CONTRACTOR_ID == contractorId) && (y.CONTRACTOR_TYPE_ID == contractorTypeId) &&
                                (y.STATE_LICENSE_ID == stateLicenseId)
                                select new { contractor = c, state = y }).First();

                    List<string> contractorlist = new List<string>();
                    contractorlist.Add("Contractor Name: " + data.state.BUSINESS_NAME);
                    contractorlist.Add("Contractor Type: " + data.state.CONTRACTOR_TYPE);
                    contractorlist.Add("License Num: " + data.state.LICENSE_NUM);
                    Session["data"] = contractorlist;

                    //ContractorLicense cLicense = new ContractorLicense();
                    var contractor = new CaseContractor();
                    sessionModel.Contractor.Add(data.contractor);
                    if (data.contractor != null)
                        contractor.ContractorEntity = data.contractor;

                    if (data.state != null)
                        contractor.ContractorLicense = data.state;

                    sessionModel.CaseContractor = contractor;
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            sessionModel = GetCaseDataDetails(sessionModel);
            Session["case"] = sessionModel;
            return Json(new { result = "Redirect", url = Url.Action("CaseDataDetails", "CreateApplication") });
        }
        */

        /// <summary>
        ///     Adds parent case object to the session model
        /// </summary>
        /// <param name="caObjId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddParentCase(decimal caObjId)
        {
            var sessionModel = (CreateApplicationViewModel)Session["case"];
            sessionModel.ParentCase.Clear();
            var data = (from c in _db.CA_OBJECT_VW
                        where c.CA_OBJECT_ID.Equals(caObjId)
                        select c).FirstOrDefault();
            sessionModel.ParentCase.Add(data);

            Session["case"] = sessionModel;
            return Json(new { result = "Redirect", url = Url.Action("CaseType", "CreateApplication") });
        }

        [HttpPost]
        public ActionResult AddPeople(List<CustomPerson> people)
        {
            var sessionModel = (CreateApplicationViewModel)Session["case"];
            sessionModel.People.PeopleList.Clear();
            foreach (var i in people)
            {
                var person = new Person
                {
                    ADDRESS_LINE1 = i.Address1,
                    ADDRESS_LINE2 = i.Address2,
                    CITY_NAME = i.City,
                    EMAIL = i.Email,
                    NAME = i.Name,
                    PHONE_HOME = i.PhoneNumber,
                    ROLE_ID = Convert.ToDecimal(i.RoleId),
                    STATE_CODE = i.State,
                    ZIP_CODE = i.ZipCode
                };
                sessionModel.People.PeopleList.Add(person);
            }

            WIPViewModel wip = new WIPViewModel();
            wip.AddPeople(sessionModel);

          /*  if (IsByPassContractorsPage(sessionModel.CaseType.SelectedCaseType))
            {
                sessionModel = GetCaseDataDetails(sessionModel);
                Session["case"] = sessionModel;
                return Json(new { result = "Redirect", url = Url.Action("CaseDataDetails", "CreateApplication") });
            }
            else {*/
                Session["case"] = sessionModel;
                return Json(new {result = "Redirect", url = Url.Action("CaseContractors", "CreateApplication")});
          //  }
        }
/*
        public bool IsByPassContractorsPage(string casetype)
        {
            return casetype == "GMLU_TYPEB" || casetype == "GMLU_TYPEA" || casetype == "GMLU_PRSUB" || casetype == "GMLU_PP" || casetype == "GMLU_LP" || casetype == "GMLU_CONC" || casetype == "GMLU_LUCC" || casetype == "GMLU_ZONE";
        }
*/
        [HttpPost]
        public ActionResult AddAddresses(List<Address> addresses, List<string> flags)
        {
            decimal flagId;
            var sessionModel = (CreateApplicationViewModel)Session["case"];
            sessionModel.Addresses.Clear();
            WIPViewModel wip = new WIPViewModel();
            //add addresses
            foreach (var address in addresses)
                sessionModel.Addresses.Add(address);
            wip.WIPAddresses(sessionModel); //add WIP addresses
            //add flags
            if (flags != null)
            {
                foreach (var flag in flags)
                {
                    flagId = GetFlagId(flag);
                    var flagItem = new CaFlagsItemBase()
                    {
                        //Flag = flag,
                        FlagId = Convert.ToInt64(flagId)
                    };
                    sessionModel.CaseFlags.Add(flagItem);
                }
                wip.AddFlags(sessionModel);
            }

            Session["case"] = sessionModel;
            return Json(new { result = "Redirect", url = Url.Action("CasePeople", "CreateApplication") });
        }

        /// <summary>
        ///     Retreives flag_id from database
        /// </summary>
        /// <param name="flag"></param>
        /// <returns></returns>
        private decimal GetFlagId(string flag)
        {
            var flagId = (from tb1 in _db.FLAGS
                          where tb1.FLAG1.Equals(flag)
                          select tb1.FLAG_ID).First();
            return flagId;
        }

        [HttpPost]
        public ActionResult GetFees(string caseNumber)
        {
            return RedirectToAction("SearchNumber", "Payments", caseNumber);
            // return Json(Url.Action("SearchNumber", "Payments", caseNumber, ));
        }

        /// <summary>
        ///     Search CONTRACTOR table based on searched name
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="selectedType">Type of the selected.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SearchContractors(string searchValue)
        {
            var contractors = from contractor in _db.CONTRACTORs
                              join state in _db.STATE_LICENSE_VW
                              on contractor.CONTRACTOR_ID equals state.CONTRACTOR_ID into state
                              from y in state.DefaultIfEmpty()
                              where
                              (contractor.BUSINESS_NAME.Contains(searchValue) || y.LICENSE_NUM.Contains(searchValue)) &&
                              y.CONTRACTOR_TYPE.Equals("CONTRACTOR")
                              select new { y };

            var sessionModel = (CreateApplicationViewModel)Session["case"];
            // sessionModel.Contractor.Clear();
            sessionModel.CaseContractor.ContractorList.Clear();
            foreach (var i in contractors)
            {
                var cl = new ContractorList
                {
                    ADDRESS_LINE1 = i.y.ADDRESS_LINE1,
                    ADDRESS_LINE2 = i.y.ADDRESS_LINE2,
                    ADDRESS_LINE3 = i.y.ADDRESS_LINE3,
                    BUSINESS_NAME = i.y.BUSINESS_NAME,
                    CITY_NAME = i.y.CITY_NAME,
                    COMMENT_TEXT = i.y.COMMENT_TEXT,
                    ContractorId = i.y.CONTRACTOR_ID,
                    EMAIL = i.y.EMAIL,
                    EXPIRED_FLAG = i.y.EXPIRED_FLAG,
                    FAX_NUMBER = i.y.FAX_NUMBER,
                    FIRST_NAME = i.y.FIRST_NAME,
                    GEN_LIABILITY = i.y.GEN_LIABILITY,
                    GEN_LIABILITY_EXP_DATE = i.y.GEN_LIABILITY_EXP_DATE,
                    LAST_NAME = i.y.LAST_NAME,
                    PHONE_HOME = i.y.PHONE_HOME,
                    PHONE_MOBILE = i.y.PHONE_MOBILE,
                    PHONE_WORK = i.y.PHONE_WORK,
                    StateLicenseId = Convert.ToInt64(i.y.STATE_LICENSE_ID),
                    STATE_CODE = i.y.STATE_CODE,
                    USER_ID = i.y.USER_ID,
                    ZIP_CODE = i.y.ZIP_CODE,
                    ContractorTypeDesc = i.y.CONTRACTOR_DESC,
                    ContractorTypeId = Convert.ToInt64(i.y.CONTRACTOR_TYPE_ID),
                    License_Num = i.y.LICENSE_NUM
                };
                cl.StateLicenseId = Convert.ToInt64(i.y.STATE_LICENSE_ID);
                cl.LICENSE_EXPIRATION_DATE = Convert.ToDateTime(i.y.LICENSE_EXPIRATION_DATE);
                sessionModel.CaseContractor.ContractorList.Add(cl);
            }

            Session["case"] = sessionModel;
            return PartialView("CaseContractorsPartial", sessionModel);
        }

        /// <summary>
        ///     Searches for permits to be added to a case as parent permits.
        /// </summary>
        /// <param name="caseNumber"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SearchParentCases(string caseNumber)
        {
            var permits = from c in _db.CA_OBJECT_VW
                          where c.CASE_NUMBER.Contains(caseNumber)
                          select c;

            var sessionModel = (CreateApplicationViewModel)Session["case"];
            sessionModel.ParentCase.Clear();
            foreach (var permit in permits)
                sessionModel.ParentCase.Add(permit);

            Session["case"] = sessionModel;
            return PartialView("CaseParentPartial", sessionModel);
        }

        /// <summary>
        ///     Search PEOPLE table based on searched name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SearchPeople(string name)
        {
            var names = from p in _db.PEOPLE
                        where p.NAME.Contains(name)
                        orderby p.NAME
                        select p;

            var roles = from t in _db.PEOPLE_ROLE
                        orderby t.ROLE_ID
                        select t;

            var sessionModel = (CreateApplicationViewModel)Session["case"];
            sessionModel.People.PeopleList.Clear();
            //List<Person> person = new List<Person>();
            foreach (var i in names)
                sessionModel.People.PeopleList.Add(i);
            //sessionModel.People.PeopleList.AddRange(person);

            foreach (var i in roles)
                sessionModel.People.Roles.Add(i);

            Session["case"] = sessionModel;
            return PartialView("CasePeoplePartial", sessionModel);
        }

        public ActionResult SearchPermit(string buildingpermit)
        {
            var casestatus =
                _db.CA_OBJECT_VW.Where(item => item.CASE_NUMBER == buildingpermit)
                    .Select(item => item.CASE_STATUS)
                    .FirstOrDefault();
            if (casestatus != null)
                return Content(casestatus, "text/plain");
            return View();
        }

        [HttpPost]
        public ActionResult InsertPlanReviewStatus(int caseobjectid, string plan_review_status)
        {
            var plan_review = new PLAN_REVIEW
            {
                CA_OBJECT_ID = caseobjectid,
                PLAN_REVIEW_STATUS = true
            };
            _db.PLAN_REVIEW.Add(plan_review);
            _db.SaveChanges();
            return View();
        }

        public ActionResult CaseStart()
        {
            if (Session["case"] == null) return View();
            var model = (CreateApplicationViewModel)Session["case"];
            return View(model);
        }

        [HttpPost]
        public ActionResult CaseStart(CreateApplicationViewModel model)
        {
            if (!ModelState.IsValid) return View();
            WIPViewModel m = new WIPViewModel();
            model = m.WIPInsert(model);
            Session["case"] = model;
            return RedirectToAction(model.ChildOrStandAlone == "Stand-Alone" ? "CaseType" : "CaseParent");
        }

        public ActionResult CaseParent()
        {
            if (Session["case"] == null) return View();
            var model = (CreateApplicationViewModel)Session["case"];
            return View(model);
        }

        public ActionResult CaseLocation()
        {
            if (Session["case"] != null)
            {
                var model = (CreateApplicationViewModel)Session["case"];
                return View(model);
            }
            return View();
        }

        /// <summary>
        ///     Step2s the stand alone.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CaseLocation(CreateApplicationViewModel model)
        {
            if (ModelState.IsValid)
            {
                Session["case"] = model;
                return RedirectToAction("CasePeople");
            }
            ModelState.AddModelError("CaseLocation.Location", "Please select an address.");
            return View();
        }

        public ActionResult CasePeople()
        {
            if (Session["case"] != null)
            {
                var model = (CreateApplicationViewModel)Session["case"];
                return View(model);
            }
            return View();
        }

        public ActionResult CaseContractors()
        {
            if (Session["case"] != null)
            {
                var model = (CreateApplicationViewModel)Session["case"];
                //  model.ContractorType.ContractorTypes = GetContractorTypes();
                model.Name = string.Empty;
                return View(model);
            }
            return View();
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public ActionResult CaseType()
        {
            if (Session["case"] != null)
            {
                var model = (CreateApplicationViewModel)Session["case"];
                // original code from pll_portal
                //model.CaseType.CaseGroups = GetCaseGroups();

                // tally code to pull case types first.
                model.CaseType.CaseTypes = GetCaseTypes();
                Session["case"] = model;
                return View(model);
            }
            return View();
        }
        
                [HttpPost]
                public ActionResult CaseType(string caseType, string subType)
                {
                    if (ModelState.IsValid)
                    {
                        var model = (CreateApplicationViewModel)Session["case"];
                        model.CaseType.SelectedCaseType = caseType;
                        model.CaseType.SelectedSubType = subType;
                        model = GetCaseTypeIds(model);
                        Session["case"] = model;
                        // wip here
                        WIPViewModel wip = new WIPViewModel();
                        wip.WIPCaseType(model);
                        if (model.ChildOrStandAlone == "Child")
                        {
                            if(model.CaseType.SelectedSubType!="GMBI_AS")
                                return Json(new {result = "Redirect", url = Url.Action("CasePeople", "CreateApplication")});
                            else
                                return Json(new { result = "Redirect", url = Url.Action("CaseContractors", "CreateApplication") });
                        }
                        if (model.CaseType.SelectedSubType != "GMBI_AS")                
                            return Json(new {result = "Redirect", url = Url.Action("CaseLocation", "CreateApplication")});
                        else
                            return Json(new { result = "Redirect", url = Url.Action("CaseContractors", "CreateApplication") });
                    }
                    return View();
                }
        /*
        [HttpPost]
        public ActionResult CaseType(string caseType, string subType)
        {
            if (ModelState.IsValid)
            {
                var model = (CreateApplicationViewModel)Session["case"];
                model.CaseType.SelectedCaseType = caseType;
                model.CaseType.SelectedSubType = subType;
                model = GetCaseTypeIds(model);
                Session["case"] = model;
                // wip here
                WIPViewModel wip = new WIPViewModel();
                wip.WIPCaseType(model);
                return Json(new { result = "Redirect", url = Url.Action("AgreementPage", "CreateApplication") });
            }
            return View();
        }
        */

        public ActionResult CaseDataDetails()
        {
            if (Session["case"] != null)
            {
                var model = (CreateApplicationViewModel)Session["case"];
                return View(model);
            }
            return View();
        }

        [HttpPost]
        public ActionResult CaseDataDetails(List<DataDetails> dataDetailsList)
        {
            if (ModelState.IsValid)
            {
                var sessionModel = (CreateApplicationViewModel)Session["case"];
                sessionModel.DataDetailsList = dataDetailsList;
                Session["case"] = sessionModel;
                Session["columnseq"] = "<ItemValue>CASE_DATA_DETAIL.LIST_VALUE_FLAG</ItemValue><ListValue><![CDATA[New,Addition,Alteration,Demolition,Move,Foundation Only,Swimming Pool,Retaining Wall,Signs,Temp Storage]]></ListValue><DetailCode><![CDATA[IMP_TYPE]]></DetailCode><DefaultData><DefaultRate><![CDATA[1]]></DefaultRate><DefaultQuantity><![CDATA[]]></DefaultQuantity><DefaultValue><![CDATA[]]></DefaultValue><Q1LabelText><![CDATA[]]></Q1LabelText><Q2LabelText><![CDATA[]]></Q2LabelText><Q3LabelText><![CDATA[]]></Q3LabelText></DefaultData>";
                WIPViewModel wip = new WIPViewModel();
                wip.AddDataDetails(sessionModel);
                return RedirectToAction("CaseCreate");
            }
            return View();
        }

        public ActionResult CaseCreate()
        {
            if (Session["case"] != null)
            {
                var model = (CreateApplicationViewModel)Session["case"];
                return View(model);
            }
            return View();
        }

        public ActionResult CaseSummary()
        {
            if (Session["case"] != null)
            {
                var model = (CreateApplicationViewModel)Session["case"];
                return View(model);
            }
            return View();
        }

        /// <summary>
        ///     Creates the case using Cityworks API
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SubmitCase()
        {
            if (ModelState.IsValid)
            {
                var user = ConfigurationManager.AppSettings["user"];
                var pw = ConfigurationManager.AppSettings["pw"];
                Factory = new Services(_serviceUri);
                var caObjectItem = new CaObjectItem();
                var model = (CreateApplicationViewModel)Session["case"];
                // Authenticate
                var authResponse =
                    Factory.Authentication(new AuthenticationService.Authenticate { LoginName = user, Password = pw });
                if (authResponse.Status == CoreResponseStatus.Ok)
                {
                    caseName = model.CaseType.SelectedCaseType;
                   
                    
                    var createCaseRequest = Factory.PLL.Case(new CaseService.Create 
                    {
                        CaseTypeId = model.CaseType.CaseTypeId,
                        SubTypeId = model.CaseType.SubTypeId,
                        //Location = model.Addresses[0].Location,
                        CaseName = model.CaObject.CASE_NAME
                    });

                    if (createCaseRequest.Status == CoreResponseStatus.Ok)
                    {
                        caObjectItem = createCaseRequest.Value;
                        var caObject = (from t in _db.CA_OBJECT
                                        where t.CA_OBJECT_ID == caObjectItem.CaObjectId
                                        select t).FirstOrDefault();
                        if (caObject == null) throw new ArgumentNullException(nameof(caObject));


                        // Add contractors
                        foreach (var contractor in model.CaseContractor.ContractorList)
                        {
                            var contractorService = Factory.PLL.CaseContractor(new CaseContractorService.Add
                            {
                                CaObjectId = Convert.ToInt64(caObject.CA_OBJECT_ID),
                                ContractorId = Convert.ToInt64(contractor.ContractorId),
                                ContractorTypeId = Convert.ToInt64(contractor.ContractorTypeId),
                                StateLicenseId = Convert.ToInt64(contractor.StateLicenseId)
                            });
                        }
                        // Add people
                        foreach (var person in model.People.PeopleList)
                        {
                            var peopleService = Factory.PLL.CasePeople(new CasePeopleService.Add
                            {
                                AddressLine1 = person.ADDRESS_LINE1,
                                AddressLine2 = person.ADDRESS_LINE2,
                                AddressLine3 = person.ADDRESS_LINE3,
                                CaObjectId = Convert.ToInt64(caObjectItem.CaObjectId),
                                CountryCode = person.COUNTRY_CODE,
                                StateCode = person.STATE_CODE,
                                Name = person.NAME,
                                PeopleId = Convert.ToInt64(person.PEOPLE_ID),
                               // CountryName = person.COUNTRY_CODE,
                                Email = person.EMAIL,
                                RoleId = Convert.ToInt64(person.ROLE_ID),
                                ZipCode = person.ZIP_CODE
                            });
                        }
                        var um = new UserManager();

                        //Add address -- Need to add once Cityworks provides a solution
                        //Addresses are added through the Cityworks Javascript API since the SDK doesn't work in 15.1
                        foreach (var flag in model.CaseFlags)
                        {
                            var caseFlagService = Factory.PLL.CaseFlags(new CaseFlagsService.Add
                            {
                                CaObjectId = Convert.ToInt64(caObjectItem.CaObjectId),
                                DateApplied = DateTime.Now,
                                FlagId = Convert.ToInt64(flag.FlagId),
                                AppliedBy = 2,
                                Severity = "FLAG"
                            });
                        }

                        // Save changes to database
                        _db.SaveChanges();
                        // Set the value of each data detail
                        SetDataDetailValues(caObjectItem.CaObjectId, model.DataDetailsList);

                        model.CaObject = caObject;
                        Session["case"] = model;

                        if (model.ParentCase.Count > 0)
                            AddChildCase(caObjectItem.CaObjectId, model.ParentCase[0]);
                    }
                }
                return RedirectToAction("CaseSummary", model);
            }
            return View();
        }

        private CreateApplicationViewModel GetCaseTypeIds(CreateApplicationViewModel model)
        {
            var caseTypeId = (from tb in _db.CASE_TYPE_VW
                              where tb.CASE_TYPE == model.CaseType.SelectedCaseType
                              select tb.CASE_TYPE_ID).FirstOrDefault();
            model.CaseType.CaseTypeId = Convert.ToInt64(caseTypeId);

            if (model.CaseType.SelectedSubType != null)
            {
                var subTypeId = (from tb in _db.SUB_TYPE
                                 where tb.SUB_TYPE1 == model.CaseType.SelectedSubType
                                 select tb.SUB_TYPE_ID).FirstOrDefault();
                model.CaseType.SubTypeId = Convert.ToInt64(subTypeId);
            }
            return model;
        }

        /// <summary>
        ///     Returns the case data details for the selected case type
        /// </summary>
        /// <param name="model">Type of the case.</param>
        /// <returns></returns>
        private CreateApplicationViewModel GetCaseDataDetails(CreateApplicationViewModel model)
        {
            decimal busCaseId;

            if (model.CaseType.SelectedSubType != null)
            {
                var subTypeId = (from tb in _db.SUB_TYPE
                                 where tb.SUB_TYPE1 == model.CaseType.SelectedSubType
                                 select tb.SUB_TYPE_ID).FirstOrDefault();
                model.CaseType.SubTypeId = Convert.ToInt64(subTypeId);
                busCaseId = (from tb in _db.BUS_CASE_VW
                             where
                             (tb.CASE_TYPE == model.CaseType.SelectedCaseType) && (tb.SUB_TYPE == model.CaseType.SelectedSubType)
                             select tb.BUS_CASE_ID).First();
            }
            else
            {
                busCaseId = (from tb in _db.BUS_CASE_VW
                             where tb.CASE_TYPE == model.CaseType.SelectedCaseType
                             select tb.BUS_CASE_ID).First();
            }

            // get data details based on bus_case_id
            var dataDetails = from tb1 in _db.DEFAULT_CASE_DATA_VW
                              join tb2 in _db.CASE_DATA_DETAIL on tb1.CASE_DATA_GROUP_ID equals tb2.CASE_DATA_GROUP_ID
                              where (tb1.BUS_CASE_ID == busCaseId) && (tb2.EXPIRED_FLAG == "N")
                              orderby tb1.SEQUENCE, tb1.GROUP_CODE, tb2.DETAIL_SEQUENCE
                              select new { tb1, tb2 };
            foreach (var detail in dataDetails)
            {
                var i = new DataDetails
                {
                    CheckboxFlag = detail.tb2.CHECKBOX_FLAG,
                    ColumnSequence = detail.tb2.COLUMN_SEQUENCE,
                    CommentFlag = detail.tb2.COMMENT_FLAG,
                    DateFlag = detail.tb2.DATE_FLAG,
                    DetailCode = detail.tb2.DETAIL_CODE,
                    DetailDesc = detail.tb2.DETAIL_DESC,
                    DetailSequence = detail.tb2.DETAIL_SEQUENCE,
                    GroupCode = detail.tb1.GROUP_CODE,
                    GroupDesc = detail.tb1.GROUP_DESC,
                    ListValuesFlag = detail.tb2.LIST_VALUES_FLAG,
                    NumberFlag = detail.tb2.NUMBER_FLAG,
                    TextFlag = detail.tb2.TEXT_FLAG,
                    ValueFlag = detail.tb2.VALUE_FLAG,
                    YesNoFlag = detail.tb2.YES_NO_FLAG
                };
                model.DataDetailsList.Add(i);
            }
            return model;
        }

        /// <summary>
        ///     Get case groups from db.
        /// </summary>
        /// <returns>
        ///     caseGroups
        /// </returns>
        private List<string> GetCaseGroups()
        {
            var caseGroups = new List<string>();
            var sql = (from a in _db.CASE_TYPE_VW
                       join b in _db.BUS_CASE_VW on a.CASE_TYPE_ID equals b.CASE_TYPE_ID
                       select a.CASE_GROUP).Distinct().OrderBy(group => group);
            foreach (var group in sql)
                caseGroups.Add(group);
            return caseGroups;
        }

        /// <summary>
        ///     Gets list of distinct contractor values
        /// </summary>
        /// <returns></returns>
        private List<string> GetContractorTypes()
        {
            var contractorTypes = new List<string>();
            var data = (from c in _db.CONTRACTOR_TYPE
                        orderby c.CONTRACTOR_TYPE1
                        select c.CONTRACTOR_TYPE1).Distinct();

            foreach (var record in data)
                contractorTypes.Add(record);

            return contractorTypes;
        }

        /// <summary>
        ///     Sets the data detail values for a case after the case is created
        /// </summary>
        /// <param name="caObjectId">The ca object identifier.</param>
        /// <param name="dataDetails">The data details.</param>
        private void SetDataDetailValues(long? caObjectId, List<DataDetails> dataDetails)
        {
            var valueType = string.Empty;
            if (caseName == "GMLU_ZONE") return;
            foreach (var details in dataDetails)
            {
                var caDataDetailVw = (from t in _db.CA_DATA_DETAIL_VW
                    where t.DETAIL_CODE.Equals(details.DetailCode) && (t.CA_OBJECT_ID == caObjectId)
                    select t).First();
                if (caDataDetailVw != null)
                {
                    // get the actual db object
                    var caDataDetail = (from t in _db.CA_DATA_DETAIL
                        where t.CA_DATA_DETAIL_ID == caDataDetailVw.CA_DATA_DETAIL_ID
                        select t).FirstOrDefault();

                    if (details.CommentFlag == "Y")
                    {
                        caDataDetail.COMMENT_VALUE = details.CommentValue;
                    }
                    else if (details.DateFlag == "Y")
                    {
                        caDataDetail.DATE_VALUE = details.DateValue;
                    }
                    else if (details.ListValuesFlag == "Y")
                    {
                        caDataDetail.LIST_VALUE = details.ListValue;
                    }
                    else if (details.NumberFlag == "Y")
                    {
                        caDataDetail.NUMBER_VALUE = details.NumberValue;
                    }
                    else if (details.ValueFlag == "Y")
                    {
                        caDataDetail.QUANTITY = details.Quantity;
                    }
                    else if (details.TextFlag == "Y")
                    {
                        caDataDetail.TEXT_VALUE = details.TextValue;
                    }
                    else if (details.YesNoFlag == "Y")
                    {
                        if (details.YesNoValue == "Yes")
                            caDataDetail.YES_NO_VALUE = "Y";
                        else if (details.YesNoValue == "No")
                            caDataDetail.YES_NO_VALUE = null;
                    }
                    else if (details.CheckboxFlag == "Y")
                    {
                        caDataDetail.YES_NO_VALUE = details.CheckboxValue ? "Y" : "N";
                    }
                }

                // save changes to db object (caDataDetail)
                _db.SaveChanges();
            }
        }

        /// <summary>
        ///     Takes the newly created CaObjectId and creates a parent case
        ///     record based on the selected parent case in the session model.
        /// </summary>
        /// <param name="caObjId"></param>
        /// <param name="parentCase"></param>
        private void AddChildCase(long? caObjId, CA_OBJECT_VW parentCase)
        {
            // get max id
            var maxId = (from c in _db.CA_CHILD_OBJECT
                         select c.CA_OBJECT_ID).Max() + 1;

            var newChild = new CA_CHILD_OBJECT
            {
                CA_OBJECT_ID = Convert.ToDecimal(caObjId),
                DATE_CREATED = DateTime.Now,
                CHILD_ID = parentCase.CA_OBJECT_ID,
                CA_CHILD_OBJECT_ID = maxId
            };
            _db.CA_CHILD_OBJECT.Add(newChild);
            _db.SaveChanges();
        }

        /// <summary>
        ///     Gets the case types.
        ///     This is a Tallahassee requirement as they do not select case groups first.
        ///     Instead they select case types and then sub-types based on the selected case type.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<SelectListItem> GetCaseTypes()
        {
            string[] caselist = new string[] { "GMBI_TENT", "CE", "GMRM_REC", "GM_MISC", "TPUD_NRES"};
            var data = (from c in _db.BUS_CASE_VW
                        where c.EXPIRED_FLAG.Equals("N") && (c.REGISTERED_FLAG.Equals("Y") || c.ANONYMOUS_FLAG.Equals("Y")) && !caselist.Contains(c.BUS_CASE_CODE)
                        select new { c.CASE_TYPE, c.CASE_TYPE_DESC, c.CASE_TYPE_ID }).Distinct();
            // List<string> groups = new List<string>();
            var groups = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = "Select Case Type",
                    Value = "Selected Case Type",
                    Disabled = true,
                    Selected = true
                }
            };

            foreach (var rec in data)
                groups.Add(new SelectListItem {Text = rec.CASE_TYPE + ": " + rec.CASE_TYPE_DESC, Value = rec.CASE_TYPE});
               // groups.Add(new SelectListItem { Text = rec.CASE_TYPE_DESC, Value = rec.CASE_TYPE });
            IEnumerable<SelectListItem> test = groups;

            return test;
        }

        [HttpPost]
        public ActionResult SearchSubType(string caseType)
        {
            var sessionModel = (CreateApplicationViewModel)Session["case"];

            var data = from b in _db.BUS_CASE_VW
                       where
                       (b.CASE_TYPE == caseType) && b.EXPIRED_FLAG.Equals("N") &&
                       (b.REGISTERED_FLAG.Equals("Y") || b.ANONYMOUS_FLAG.Equals("Y")) && !b.BUS_CASE_CODE.Equals("GMLU_CNTYC")
                       orderby b.SUB_TYPE
                       select new { b };
            var subTypeList = new List<SelectListItem>();

            if (data.Any(m => m.b.SUB_TYPE != null))
            {
                subTypeList.Add(new SelectListItem
                {
                    Text = "Select Sub-Type",
                    Value = "Selected Sub-Type",
                    Disabled = true,
                    Selected = true
                });

                foreach (var rec in data)
                    subTypeList.Add(new SelectListItem
                    {
                        Text = rec.b.SUB_TYPE + ": " + rec.b.SUB_TYPE_DESC,
                       // Text = rec.b.SUB_TYPE_DESC,
                        Value = rec.b.SUB_TYPE
                    });
            }
            sessionModel.CaseType.SubTypes = subTypeList;
            Session["case"] = sessionModel;
            return PartialView("CaseTypePartial", sessionModel);
        }
    }
}