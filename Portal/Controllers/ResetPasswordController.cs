﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Portal.Models.ViewModel;
using Portal.Models.DB;
using Portal.Models.EntityManager;
using System.Data.Entity.Validation;


namespace Portal.Controllers
{
    [Authorize]
    public class ResetPasswordController : Controller
    {
        private CityworksEntities db = new CityworksEntities();
        // GET: ResetPassword
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult GetQuestion(string username)
        {
            var question = (from p in db.PAA_EXTERNAL_USERS
                        where p.LOGIN_ID.ToUpper().Equals(username.ToUpper())
                        select p.SECURITY_QUESTION).FirstOrDefault();
            if (question == null)
            {
                ModelState.AddModelError("Username", "Username not found.");
                return Json("Error");
            }

            return Json(question);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult GetAnswer(string answer, string username)
        {
            var userAnswer = (from p in db.PAA_EXTERNAL_USERS
                                    where p.LOGIN_ID.ToUpper().Equals(username.ToUpper())
                                    select p.ANSWER).FirstOrDefault();
            if (userAnswer.ToUpper().Equals(answer.ToUpper()))
            {

                return Json("Success");
            }
            else
            {
                return Json("Error");
            }
        }

        [AllowAnonymous]
        public ActionResult Reset(ResetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var user = (from p in db.USERS
                            where p.LOGIN_ID.ToUpper().Equals(model.Username.ToUpper())
                            select p).FirstOrDefault();

                UserManager um = new UserManager();
                string password = um.GetMD5Hash(model.Password);
                user.PASSWORD = password;

                try
                {
                    db.SaveChanges();
                    return View("Index");
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var entityValidationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in entityValidationErrors.ValidationErrors)
                        {
                            Response.Write("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                        }
                    }
                }
            }

            return View("Index");
       }
    }
}