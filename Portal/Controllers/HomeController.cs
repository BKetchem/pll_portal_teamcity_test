﻿using System.Web.Mvc;
using System.Security.Claims;

namespace Portal.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        [Authorize]
        public ActionResult Index()
        {
           
            return View();
        }

        public ActionResult Welcome()
        {
          
            return View();
        }
    }
}