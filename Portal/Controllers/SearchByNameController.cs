﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Portal.Models.DB;

namespace Portal.Controllers
{
    public class SearchByNameController : Controller
    {
        private CityworksEntities db = new CityworksEntities();
        // GET: SearchByName
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SearchName()
        {
            return PartialView("_SearchName");
        }

        [HttpPost]
        public PartialViewResult SearchName(string name)
        {
            
            var tab = (from p in db.CA_PEOPLE_VW
                       where p.NAME.Contains(name)
                       select p).ToList();
          /*  if(tab.Count() == 0)
            {
                var tab1 = (from c in db.CA_CONTRACTOR_VW
                       where c.LICENSE_NUM.Contains(name)
                       select c).ToList();
                      
            return PartialView("_SearchLicenseResults", tab1);
            } */
            return PartialView("_SearchNameResults", tab);
        }
    }
}