﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Portal.Models.ViewModel;
using Portal.Models.DB;
using Portal.Models.EntityManager;
using System.Data.Entity.Validation;
using Cityworks.Core;

namespace Portal.Controllers
{
    [Authorize]
    public class CreateAccountController : Controller
    {
        private CityworksEntities db = new CityworksEntities();
        // GET: CreateAccount
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Create(CreateUserModel model)
        {
            if (ModelState.IsValid)
            {
                //  decimal maxId = db.PAA_EXTERNAL_USERS.Max(u => u.EXT_USER_ID);
                var t = (from tb1 in db.PAA_EXTERNAL_USERS
                         select tb1.EXT_USER_ID ).First();
                var username = from u in db.PAA_EXTERNAL_USERS
                                    select u.LOGIN_ID;
                foreach (var name in username)
                {
                    if (name.ToString() == model.ExternalUser.LOGIN_ID)
                    {
                        
                        //return View("Index", model);
                        return Json("Error");
                    }
                }
                ////decimal maxId = Convert.ToDecimal(t);
                UserManager um = new UserManager();
                string password = um.GetMD5Hash(model.Password);
                PAA_EXTERNAL_USERS user = new PAA_EXTERNAL_USERS
                {
                    LOGIN_ID = model.ExternalUser.LOGIN_ID,
                    ADDRESS_LINE1 = model.ExternalUser.ADDRESS_LINE1,
                    ADDRESS_LINE2 = model.ExternalUser.ADDRESS_LINE2,
                    ANSWER = model.ExternalUser.ANSWER,
                    CITY_NAME = model.ExternalUser.CITY_NAME,
                    COUNTRY_CODE = model.ExternalUser.COUNTRY_CODE,
                    FIRST_NAME = model.ExternalUser.FIRST_NAME,
                    LAST_NAME = model.ExternalUser.LAST_NAME,
                    PASSWORD = password,
                    PHONE_MOBILE = model.ExternalUser.PHONE_MOBILE,
                    PHONE_WORK = model.ExternalUser.PHONE_WORK,
                    SECURITY_QUESTION = model.ExternalUser.SECURITY_QUESTION,
                    STATE_CODE = model.ExternalUser.STATE_CODE,
                    STATE_LICENSE_NUMBER = model.ExternalUser.STATE_LICENSE_NUMBER,
                    ZIP_CODE = model.ExternalUser.ZIP_CODE,
                    APPROVE_FLAG = "N",
                    DATE_CREATED = DateTime.Now,
                    EXT_USER_ID = t + 1
                };

                db.PAA_EXTERNAL_USERS.Add(user);

                try
                {
                    db.SaveChanges();
                    return Json("Success");
                }
                catch (DbEntityValidationException ex)
                {
                    foreach (var entityValidationErrors in ex.EntityValidationErrors)
                    {
                        foreach (var validationError in entityValidationErrors.ValidationErrors)
                        {
                            Response.Write("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                        }
                    }
                }
            }

            return View("Index", model);
        }
    }
}