﻿using Cityworks.Core;
using Portal.Models.DB;
using Portal.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;

namespace Portal.Controllers
{
    public class InspectionRequestsController : Controller
    {
        private CityworksEntities db = new CityworksEntities();
        private const string SESSION_SERVICE_FACTORY = "ServiceFactory";
        private Uri serviceUri = new Uri(ServerUrl);
        public static string ServerUrl = ConfigurationManager.AppSettings["servicesUrl"];

        private Services factory
        {
            get { return (Services)Session[SESSION_SERVICE_FACTORY]; }
            set { Session[SESSION_SERVICE_FACTORY] = value; }
        }

        public object CaObjectId { get; private set; }

        // GET: InspectionRequests
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetInspections()
        {
            InspectionRequestModel model = new InspectionRequestModel();
            var id = (ClaimsIdentity)User.Identity;
            var userid = 0;
            var claims = ClaimsPrincipal.Current.Claims.ToList();
            for (var i = 0; i < claims.Count(); i++)
            {
                if (i == 1)
                    userid = Convert.ToInt32(claims[i].Value);
            }

            var tab = (from table1 in db.PLL_INSPECTION_REQUESTS
                       join table2 in db.CA_OBJECT_VW
                       on table1.CA_OBJECT_ID equals table2.CA_OBJECT_ID
                       join table3 in db.CA_TASK_VW
                       on table1.CA_TASK_ID equals table3.CA_TASK_ID
                       where table1.USER_ID.Value.Equals(userid)
                       && !table1.TASK_CODE.Equals("930") && !table1.TASK_CODE.Equals("601") && !table1.TASK_CODE.Equals("603") // filter these task codes
                       && !table2.CASE_TYPE.Equals("TLCPD_TPZ") && !table2.CASE_TYPE.Equals("TLCPD_TPUD") && !table2.CASE_TYPE.Equals("TLCPD_TTX") //filter these case types
                       && !table2.CASE_TYPE.Equals("TLCPD_TYPC") && !table2.CASE_TYPE.Equals("GMLU_OP") && !table2.CASE_TYPE.Equals("GMLU_OPC")
                       && !table2.CASE_TYPE.Equals("GMLU_OPR") && !table2.CASE_TYPE.Equals("GMCE_CODE")
                       && !table2.SUB_TYPE.Equals("GMBI_ANFM") && !table2.SUB_TYPE.Equals("GMBI_ANFG") // filter these sub-types
                       && table3.TASK_AVAILABLE_FLAG.Equals("Y") && table3.EXPIRED_FLAG.Equals("N") && table3.TARGET_END_DATE.Equals(null)
                       select new { table1, table2 });

            foreach (var table in tab)
            {
                Inspection_Requests req = new Inspection_Requests();
                req.CASE_NUMBER = table.table1.CASE_NUMBER;
                req.CASE_TYPE = table.table1.CASE_TYPE;
                req.CA_OBJECT_ID = table.table1.CA_OBJECT_ID;
                req.CA_TASK_ID = table.table1.CA_TASK_ID;
                req.LOCATION = table.table1.LOCATION;
                req.SUB_TYPE = table.table2.SUB_TYPE;
                req.TASK_CODE = table.table1.TASK_CODE;
                req.TASK_DESC = table.table1.TASK_DESC;
                model.Inspection_Requests.Add(req);
            }
            return PartialView("_GetInspections", model);
        }

        [HttpPost]
        public ActionResult ShowInspection(Inspection_Requests schedRequest)
        {
            InspectionRequestModel model = new InspectionRequestModel();
            //var inspRequest = (from i in db.PLL_INSPECTION_REQUESTS
            //                   where i.CA_OBJECT_ID.Equals(schedRequest.CA_OBJECT_ID) && i.CA_TASK_ID.Equals(schedRequest.CA_TASK_ID)
            //                   select i).FirstOrDefault();
            var dataDetails = (from table in db.CA_DATA_DETAIL_VW
                               where table.CA_OBJECT_ID.Equals(schedRequest.CA_OBJECT_ID)
                               select table).ToList();
            if (schedRequest != null)
            {
                //var days = DateTimeExtensions.GetWorkDays();
                //model.NextFiveDays = days;

                model.Inspection_Requests.Clear(); //clear search results
                model.Inspection_Requests.Add(schedRequest); //add selected result

                if (dataDetails.Count() > 0)
                {
                    model.CaseDataDetails = dataDetails;
                }

                Session["model"] = model;
                return Json(new { result = "Redirect", url = Url.Action("DisplayInspectionResult", "InspectionRequests") });
            }
            else
                return Json(new { result = "Fail" });
        }

        public ActionResult DisplayInspectionResult()
        {
            InspectionRequestModel model = (InspectionRequestModel)Session["model"];
            return PartialView("_InspectionResult", model);
        }

        [HttpPost]
        public ActionResult GetHolidays()
        {
            var tab = (from h in db.HOLIDAYS_VW
                       select h.HOLIDAY_DATE).ToList().Select(d => d.ToString("MM/dd/yyyy"));
            List<string> holidays = new List<string>();
            foreach (var t in tab)
            {
                holidays.Add(t.ToString());
            }

            return Json(holidays);
        }

        private bool IsAMPMInspection(InspectionRequestModel model)
        {
            bool response = false;

            var inClause = new string[] { "" };
            if (model.Inspection_Requests[0].TASK_CODE == "300" || model.Inspection_Requests[0].TASK_CODE == "301" || model.Inspection_Requests[0].TASK_CODE == "903")
            {
                var result = (from t1 in model.CaseDataDetails
                              join t2 in model.CaseDataDetails on t1.CA_OBJECT_ID equals t2.CA_OBJECT_ID
                              where (t1.DETAIL_CODE == "imp_type" && (t1.LIST_VALUE == "new" || t1.LIST_VALUE == "addition")) && (t2.DETAIL_CODE == "bldg_class" && (t2.LIST_VALUE == "one family detached"
                              || t2.LIST_VALUE == "two family (duplex)" || t2.LIST_VALUE == "one family attached (duplex)" || t2.LIST_VALUE == "carport/garage one & two family" || t2.LIST_VALUE == "mobile home"))
                              select t1.CA_OBJECT_ID).FirstOrDefault();
                if (result > 0)
                {
                    response = true;
                }
            }
            return response;
        }

        [HttpPost]
        public ActionResult GetAvailableTimes(string TaskId, string TaskCode, string CaObjectId, DateTime InspectionDate)
        {
            List<AvailableTimes> tl = new List<AvailableTimes>();
            InspectionRequestModel sessionModel = (InspectionRequestModel)Session["model"];
            List<string> dates = new List<string>();
            List<DateTimeRange> times = new List<DateTimeRange>();
            InspectionDate = InspectionDate.Date;
            string caseGroup = string.Empty;
            switch (Convert.ToInt32(TaskCode))
            {
                case 300:
                case 301:
                case 302:
                case 303:
                case 304:
                case 305:
                case 306:
                case 307:
                case 308:
                case 309:
                case 310:
                case 502:
                case 503:
                case 902:
                case 903:
                case 925:
                    caseGroup = "MG";
                    break;

                case 908:
                case 911:
                case 912:
                case 913:
                case 914:
                case 915:
                case 934:
                case 935:
                    caseGroup = "F";
                    break;
            }
            if ((caseGroup == "MG" || caseGroup == "F") && !IsAMPMInspection(sessionModel))
            {
                var takenDates = (from t in db.INSP_SCHEDULER
                                  where t.GROUP.Equals(caseGroup) && t.TARGET_START_DATE >= InspectionDate
                                  select new { t.TARGET_START_DATE, t.TARGET_END_DATE }).ToList();
                for (var i = 0; i <= 4; i++)
                {
                    DateTimeRange range = new DateTimeRange();
                    switch (i)
                    {
                        case 0:
                            range.Start = InspectionDate + new TimeSpan(08, 30, 00);
                            range.End = InspectionDate + new TimeSpan(09, 30, 00);
                            times.Add(range);
                            break;

                        case 1:
                            range.Start = InspectionDate + new TimeSpan(09, 30, 00);
                            range.End = InspectionDate + new TimeSpan(10, 30, 00);
                            times.Add(range);
                            break;

                        case 2:
                            range.Start = InspectionDate + new TimeSpan(10, 30, 00);
                            range.End = InspectionDate + new TimeSpan(11, 30, 00);
                            times.Add(range);
                            break;

                        case 3:
                            range.Start = InspectionDate + new TimeSpan(13, 00, 00);
                            range.End = InspectionDate + new TimeSpan(14, 00, 00);
                            times.Add(range);
                            break;

                        case 4:
                            range.Start = InspectionDate + new TimeSpan(14, 00, 00);
                            range.End = InspectionDate + new TimeSpan(15, 00, 00);
                            times.Add(range);
                            break;
                    }
                }

                foreach (var available in times)
                {
                    if (takenDates.Count() > 0)
                    {
                        foreach (var i in takenDates)
                        {
                            DateTimeRange t = new DateTimeRange();
                            t.Start = Convert.ToDateTime(i.TARGET_START_DATE);
                            t.End = Convert.ToDateTime(i.TARGET_END_DATE);
                            var v = t.Intersects(available);
                            if (!v)
                            {
                                AvailableTimes test = new AvailableTimes();
                                test.Start = available.Start.ToString();
                                test.End = available.End.ToString();
                                tl.Add(test);
                            }
                        }
                    }
                    else
                    {
                        AvailableTimes test = new AvailableTimes();
                        test.Start = available.Start.ToString();
                        test.End = available.End.ToString();
                        tl.Add(test);
                    }
                }
            }
            else
            {
                for (var i = 0; i <= 3; i++)
                {
                    DateTimeRange range = new DateTimeRange();
                    switch (i)
                    {
                        case 0:
                            range.Start = InspectionDate + new TimeSpan(08, 00, 00);
                            range.End = InspectionDate + new TimeSpan(12, 00, 00);
                            times.Add(range);
                            break;

                        case 1:
                            range.Start = InspectionDate + new TimeSpan(13, 00, 00);
                            range.End = InspectionDate + new TimeSpan(15, 00, 00);
                            times.Add(range);
                            break;

                        case 2:
                            range.Start = InspectionDate + new TimeSpan(08, 00, 00);
                            range.End = InspectionDate + new TimeSpan(15, 00, 00);
                            times.Add(range);
                            break;
                    }
                }

                foreach (var available in times)
                {
                    AvailableTimes test = new AvailableTimes();
                    test.Start = available.Start.ToString();
                    test.End = available.End.ToString();
                    tl.Add(test);
                }
            }

            return Json(tl);
        }

        private TimeSpan GetTimeFromHTML(string time)
        {
            DateTime t;
            DateTime.TryParse(time, out t);
            TimeSpan span = t.TimeOfDay;
            return span;
        }

        [HttpPost]
        public ActionResult ScheduleInspection(DateTime Day, string Time, string Comments)
        {
            InspectionRequestModel sessionModel = (InspectionRequestModel)Session["model"];
            AvailableTimes t = new AvailableTimes();
            string user = ConfigurationManager.AppSettings["user"];
            string pw = ConfigurationManager.AppSettings["pw"];
            factory = new Services(serviceUri);
            string requestor = User.Identity.Name;
            long? confirmationId;
            string[] times = Time.Split('-');
            t.Start = times[0];
            t.End = times[1];
            var start = GetTimeFromHTML(t.Start);
            var end = GetTimeFromHTML(t.End);

            var authResponse = factory.Authentication(new AuthenticationService.Authenticate { LoginName = user, Password = pw });
            if (authResponse.Status == CoreResponseStatus.Ok)
            {
                var inspectionRequest = factory.PLL.CaseInspectionRequest(new CaseInspectionRequestService.Add
                {
                    CaObjectId = Convert.ToInt64(sessionModel.Inspection_Requests[0].CA_OBJECT_ID),
                    RequestorComment = Comments,
                    RequestorName = requestor,
                    RequestSource = "PORTAL"
                });

                if (inspectionRequest.Status == CoreResponseStatus.Ok)
                {
                    confirmationId = inspectionRequest.Value.ConfirmationId;
                }

                var taskUpdate = factory.PLL.CaseTask(new CaseTaskService.Update
                {
                    CaTaskId = Convert.ToInt64(sessionModel.Inspection_Requests[0].CA_TASK_ID),
                    TargetStartDate = Day + start,
                    TargetEndDate = Day + end
                });

                var commentUpdate = factory.PLL.CaseTaskComments(new CaseTaskCommentsService.Add
                {
                    CaTaskId = Convert.ToInt64(sessionModel.Inspection_Requests[0].CA_TASK_ID),
                    CommentText = Comments
                });
                sessionModel.Comments = Comments;
                sessionModel.ScheduledDate = taskUpdate.Value.TargetStartDate.ToString() + " - " + taskUpdate.Value.TargetEndDate.ToString();
                Session["model"] = sessionModel;
            }
            //return PartialView("_InspectionSummary", sessionModel);
            return Json(new { result = "Redirect", url = Url.Action("InspectionSummary", "InspectionRequests") });
        }

        public ActionResult InspectionSummary()
        {
            if (Session["model"] != null)
            {
                InspectionRequestModel model = (InspectionRequestModel)Session["model"];
                return View(model);
            }
            return View();
        }
    }
}