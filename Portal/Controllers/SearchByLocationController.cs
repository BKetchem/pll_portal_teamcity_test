﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Portal.Models.DB;

namespace Portal.Controllers
{
    public class SearchByLocationController : Controller
    {
        private CityworksEntities db = new CityworksEntities();
        // GET: SearchByLocation
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SearchLocation()
        {
            return PartialView("_SearchLocation");
        }

        [HttpPost]
        public PartialViewResult SearchLocation(string location)
        {
            var tab = (from p in db.CA_OBJECT_VW
                       where p.LOCATION.Contains(location)
                       select p).ToList();
            return PartialView("_SearchLocationResults", tab);
        }
    }
}