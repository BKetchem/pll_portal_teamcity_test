﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Portal.Models.EntityManager;
using Portal.Models.ViewModel;
using System.Web.Security;
using System.Security.Claims;

namespace Portal.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        // GET: Account
        
        public ActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult LogIn()
        {
            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult LogIn(UserLoginView ULV, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                UserManager UM = new UserManager();
                string encryptedPW = UM.GetMD5Hash(ULV.Password);
                string password = UM.GetUserPassword(ULV.LoginName);
                ULV.USER_ID = UM.GetUserId(ULV.LoginName);
                if (ULV.USER_ID == 0)
                {
                    ModelState.AddModelError("LoginName", "User not found.");
                    return View("LogIn");
                }

               
                else
                {
                    if (encryptedPW.Equals(password))
                    {
                        ULV.Role = UM.GetUserRole(ULV.USER_ID);
                        //FormsAuthentication.SetAuthCookie(ULV.LoginName, false);
                        var identity = new ClaimsIdentity(new[] {
                            new Claim(ClaimTypes.Name, ULV.LoginName),
                            new Claim(ClaimTypes.NameIdentifier, ULV.USER_ID.ToString()),
                            new Claim(ClaimTypes.Role, ULV.Role)
                        },
                         "ApplicationCookie");

                        var ctx = Request.GetOwinContext();
                        var authManager = ctx.Authentication;

                        authManager.SignIn(identity);

                        return Redirect(GetRedirectUrl(ULV.ReturnUrl));
                        //return RedirectToAction("Welcome", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError("Password", "The password is incorrect.");
                        return View("LogIn");
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(ULV);
        }

        private string GetRedirectUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                return Url.Action("welcome", "home");
            }

            return returnUrl;
        }

        public ActionResult LogOut()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("LogIn", "Account");
        }

       
    }
}