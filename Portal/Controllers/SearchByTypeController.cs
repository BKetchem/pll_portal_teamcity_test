﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Portal.Models.DB;
using Portal.Models.ViewModel;
using System.Web.Services;
namespace Portal.Controllers
{
    public class SearchByTypeController : Controller
    {
        private CityworksEntities db = new CityworksEntities();
        // GET: SearchByName
        public ActionResult Index()
        {
            CaseTypes model = new CaseTypes();
            model.CaseTypeList = GetCaseTypes();
            Session["type"] = model;
            return View(model);
        }

        //[HttpGet]
        //public ActionResult SearchType()
        //{
        //    //CaseTypes model = new CaseTypes();
        //    //model.CaseTypeList = GetCaseTypes();
        //    //Session["type"] = model;
        //    //return PartialView("_SearchType",  model);
        //}


        public IEnumerable<SelectListItem> GetCaseTypes()
        {
            CaseTypes model = (CaseTypes)Session["type"];
            var data = from c in db.CASE_TYPE
                       orderby c.CASE_TYPE1
                       select new { c.CASE_TYPE1, c.CASE_TYPE_DESC, c.CASE_TYPE_ID };
            // List<string> groups = new List<string>();
            List<SelectListItem> groups = new List<SelectListItem>();

            groups.Add(new SelectListItem() { Text = "Select Case Type", Value = "Selected Case Type", Disabled = true, Selected = true });

            foreach (var rec in data)
            {
                groups.Add(new SelectListItem() { Text = rec.CASE_TYPE1 + ": " + rec.CASE_TYPE_DESC, Value = rec.CASE_TYPE1 });
            }

            IEnumerable<SelectListItem> test = groups;
            //Session["type"] = model;
            return test;
        }

        [HttpPost]
        public ActionResult SearchSubType(string caseType)
        {
            CaseTypes sessionModel = (CaseTypes)Session["type"];

            var data = (from b in db.BUS_CASE_VW
                        where b.CASE_TYPE == caseType
                        orderby b.SUB_TYPE
                        select new { b = b });
            List<SelectListItem> subTypeList = new List<SelectListItem>();

            if (data.Any(m => m.b.SUB_TYPE != null))
            {
                subTypeList.Add(new SelectListItem { Text = "Select Sub-Type", Value = "Selected Sub-Type", Disabled = true, Selected = true });

                foreach (var rec in data)
                {
                    subTypeList.Add(new SelectListItem() { Text = rec.b.SUB_TYPE + ": " + rec.b.SUB_TYPE_DESC, Value = rec.b.SUB_TYPE });
                }
            }
            sessionModel.SubTypeList = subTypeList;
            Session["type"] = sessionModel;
            return PartialView("CaseTypePartial", sessionModel);
        }

       

        [HttpPost]
        public ActionResult Search(string permitType, string subType, DateTime? fromDateInput, DateTime? toDateInput)
        {
            CaseTypes sessionModel = (CaseTypes)Session["type"];

            if (fromDateInput != null && toDateInput != null)
            {
                sessionModel.CaObject = (from p in db.CA_OBJECT_VW
                         where p.CASE_TYPE.Contains(permitType) && p.DATE_CREATED >= fromDateInput && p.DATE_CREATED <= toDateInput
                         select p).ToList();
            }

            else if (subType != "")
            {
                sessionModel.CaObject = (from p in db.CA_OBJECT_VW
                         where p.CASE_TYPE.Contains(permitType) && p.SUB_TYPE.Contains(subType)
                         select p).ToList();
            }
            else
            {
                sessionModel.CaObject = (from p in db.CA_OBJECT_VW
                         where p.CASE_TYPE.Contains(permitType)
                         select p).ToList();
            }

            Session["type"] = sessionModel;
            return PartialView("_SearchTypeResults", sessionModel);
        }
    }
}