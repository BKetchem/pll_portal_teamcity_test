﻿using System.Data;
using System.Linq;
using System.Web.Mvc;
using Portal.Models.DB;


namespace Portal.Controllers
{
    public class ReviewApplicationsController : Controller
    {
        private CityworksEntities db = new CityworksEntities();

        // GET: ReviewApplications
        public ActionResult OpenPermits()
        {
            var tab = db.CA_OBJECT_VW
                .Where(p => p.CREATED_BY_LOGIN_ID == HttpContext.User.Identity.Name && p.CASE_STATUS == "OPEN" );
            return View(tab);
        }

        public ActionResult IncompletePermits()
        {
            var tab = db.WIP_CA_OBJECT_VW
                .Where(p => p.CREATED_BY_LOGIN_ID == HttpContext.User.Identity.Name);
            return View(tab);
        }

       
    }
}
