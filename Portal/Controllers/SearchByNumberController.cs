﻿using Portal.Models.DB;
using System.Linq;
using System.Web.Mvc;

namespace Portal.Controllers
{
    public class SearchByNumberController : Controller
    {
        private CityworksEntities db = new CityworksEntities();

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult PermitNumber()
        {
            return PartialView("_PermitNumberFormPartial");
            // return View();
        }

        [HttpPost]
        public PartialViewResult PermitNumber(string permitNumber)
        {
            var tab = from c in db.CA_OBJECT_VW
                      where c.CASE_NUMBER.Contains(permitNumber)
                      select c;
            return PartialView("_PermitNumberResultsPartial", tab);
        }
    }
}